﻿using System.Security.Claims;
using Tilde.MT.WebsiteTranslationService.Exceptions;
using Tilde.MT.WebsiteTranslationService.Exceptions.Translation;
using Tilde.MT.WebsiteTranslationService.Models.DTO;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Translation;

namespace Tilde.MT.WebsiteTranslationService.Interfaces.Services
{
    public interface ISegmentService
    {
        /// <summary>
        /// List all segments
        /// </summary>
        /// <param name="user"> User associated with the executing action </param>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="filters"> Filters for segments </param>
        /// <returns></returns>
        public Task<SegmentList> ListSegments(ClaimsPrincipal user, Guid websiteId, SegmentListFilter filters);

        /// <summary>
        /// List all translations
        /// </summary>
        /// <param name="user"> User associated with the executing action </param>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="segmentId"> Segment unique identifier </param>
        /// <param name="targetLanguage"> Target language </param>
        /// <returns></returns>
        public Task<IEnumerable<Translation>> ListTranslations(ClaimsPrincipal user, Guid websiteId, long segmentId, string targetLanguage);

        /// <summary>
        /// Delete segment
        /// </summary>
        /// <param name="user"> User associated with the executing action </param>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="segmentId"> Segment unique identifier </param>
        /// <returns></returns>
        /// <exception cref="ResourceNotFoundException"></exception>
        public Task DeleteSegment(ClaimsPrincipal user, Guid websiteId, long segmentId);

        /// <summary>
        /// Add translation
        /// </summary>
        /// <param name="user"> User associated with the executing action </param>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="sourceSegmentId"> Source segment unique identifier </param>
        /// <param name="targetLanguage"> Target language </param>
        /// <param name="suggestion"> New text suggestion </param>
        /// <param name="uri"> URI </param>
        /// <returns></returns>
        /// <exception cref="ResourceNotFoundException"></exception>
        /// <exception cref="InvalidTranslationException"></exception>
        public Task<Translation> Add(ClaimsPrincipal user, Guid websiteId, long sourceSegmentId, string targetLanguage, string suggestion, string uri);

        /// <summary>
        /// Make translation as accepted
        /// </summary>
        /// <param name="user"> User associated with the executing action </param>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="lang"> Target language </param>
        /// <param name="sourceSegmentId"> Source segment unique identifier </param>
        /// <param name="suggestionId"> Suggestion unique identifier </param>
        /// <returns></returns>
        /// <exception cref="ResourceNotFoundException"></exception>
        public Task Accept(ClaimsPrincipal user, Guid websiteId, string lang, long sourceSegmentId, long suggestionId);

        /// <summary>
        /// Remove translation
        /// </summary>
        /// <param name="user"> User associated with the executing action </param>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="sourceSegmentId"> Source segment unique identifier </param>
        /// <param name="language"> Target language </param>
        /// <param name="suggestionId"> Suggestion unique identifier </param>
        /// <returns></returns>
        /// <exception cref="ResourceNotFoundException"></exception>
        /// <exception cref="AcceptedTranslationDeleteNotAllowedException"></exception>
        public Task Remove(ClaimsPrincipal user, Guid websiteId, long sourceSegmentId, string language, long suggestionId);
    }
}
