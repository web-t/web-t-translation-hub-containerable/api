﻿using Tilde.EMW.Contracts.Exceptions.TranslationSystem;

namespace Tilde.MT.WebsiteTranslationService.Interfaces.Services
{
    public interface IMachineTranslationService
    {
        /// <summary>
        /// Translate text with Machine Translation
        /// </summary>
        /// <param name="sourceLanguage"> Source language </param>
        /// <param name="targetLanguage"> Target language </param>
        /// <param name="domain"> Domain name </param>
        /// <param name="text"> Texts to translate </param>
        /// <returns></returns>
        /// <exception cref="LanguageDirectionNotFoundException"></exception>
        /// <exception cref="TranslationTimeoutException"></exception>
        public Task<IEnumerable<string>> Translate(string sourceLanguage, string targetLanguage, string domain, IEnumerable<string> text);
    }
}
