﻿using System.Security.Claims;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Page;

namespace Tilde.MT.WebsiteTranslationService.Interfaces.Services
{
    public interface IPageService
    {
        /// <summary>
        /// List available pages for website
        /// </summary>
        /// <param name="user"> User associated with the executing action </param>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="filters"> Filters for pages </param>
        /// <returns></returns>
        Task<PageList> ListPages(ClaimsPrincipal user, Guid websiteId, Models.DTO.Page.PageListFilter filters);
    }
}
