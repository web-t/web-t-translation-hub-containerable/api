﻿using Tilde.MT.WebsiteTranslationService.Exceptions;
using Tilde.MT.WebsiteTranslationService.Exceptions.Domain;

namespace Tilde.MT.WebsiteTranslationService.Interfaces.Services
{
    public interface ITranslationService
    {
        /// <summary>
        /// Translate text
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="request"> Segment request </param>
        /// <param name="referer"> Referer header of the HTTP request </param>
        /// <returns></returns>
        /// <exception cref="DomainNotAllowedException"></exception>
        /// <exception cref="ResourceNotFoundException"></exception>
        public Task<IEnumerable<Models.DTO.Translation.TranslationItem>> Translate(Guid websiteId, Models.DTO.Translation.SegmentRequest request, Uri? referer);
    }
}
