﻿using Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;

namespace Tilde.MT.WebsiteTranslationService.Interfaces.Services
{
	public interface IConfigurationService
	{
		/// <summary>
		/// Get configuration by unique identifier
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <returns></returns>
		public Task<Configuration> Get(Guid id);
	}
}
