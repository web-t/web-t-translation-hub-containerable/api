﻿namespace Tilde.MT.WebsiteTranslationService.Interfaces.Providers
{
    public interface ISegmentNormalizerProvider
    {
        /// <summary>
        /// CanSearch text for storage and search purposes without style or other user specific data
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        string Normalize(string text);
    }
}
