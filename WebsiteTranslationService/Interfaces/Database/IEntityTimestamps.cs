﻿namespace Tilde.MT.WebsiteTranslationService.Interfaces.Database
{
    /// <summary>
    /// Interface for entity timestamps
    /// </summary>
    public interface IEntityTimestamps
    {
        /// <summary>
        /// Timestamp when entity is created in database
        /// </summary>
        DateTime? DbCreatedAt { get; set; }

        /// <summary>
        /// Timestamp when entity is updated in database
        /// </summary>
        DateTime? DbUpdatedAt { get; set; }
    }
}
