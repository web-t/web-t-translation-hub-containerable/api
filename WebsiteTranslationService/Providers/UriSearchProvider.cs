﻿namespace Tilde.MT.WebsiteTranslationService.Providers
{
    public class UriSearchProvider
    {
        /// <summary>
        /// CanSearch text for storage and search purposes without style or other user specific data
        /// </summary>
        /// <param name="uri"> URI </param>
        /// <param name="allowedDomains"> Allowed domains list </param>
        /// <param name="normalizedUri"> Normalized URI </param>
        /// <returns></returns>
        public bool CanSearch(string uri, IEnumerable<string> allowedDomains, out string? normalizedUri)
        {
            var uriSearch = uri.Trim();
            var shouldCheckDomain = true;
            try
            {
                var parsedUri = new Uri(uriSearch, UriKind.RelativeOrAbsolute);
                if (!parsedUri.IsAbsoluteUri)
                {
                    if (uriSearch.StartsWith('/'))
                    {
                        // Can be something like this without protocol: example.com/foo/bar
                        parsedUri = new Uri($"http://domain{uriSearch}", UriKind.RelativeOrAbsolute);
                        shouldCheckDomain = false;
                    }
                    else
                    {
                        // Can be something like this without protocol: example.com/foo/bar
                        parsedUri = new Uri($"http://{uriSearch}", UriKind.RelativeOrAbsolute);
                    }
                }

                if (parsedUri.IsAbsoluteUri)
                {
                    uriSearch = parsedUri.AbsolutePath;

                    if (shouldCheckDomain && !allowedDomains.Select(x => new Uri(x).DnsSafeHost).Contains(parsedUri.DnsSafeHost))
                    {
                        normalizedUri = null;
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                // If this can not be parsed as relative url, then leave it as is for search
            }
            normalizedUri = uriSearch;
            return true;
        }
    }
}
