﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Tilde.MT.WebsiteTranslationService.Interfaces.Providers;

namespace Tilde.MT.WebsiteTranslationService.Providers
{
    public class SegmentNormalizerProvider : ISegmentNormalizerProvider
    {

        /// <inheritdoc/>
        public string Normalize(string text)
        {
            var result = text.Trim().ToLower();
            result = Regex.Replace(result, @"\s+", " ");

            using SHA256 mySHA256 = SHA256.Create();
            var hashValue = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(result));

            // MySQL InnoDB index limit 3072 bytes, so hash it
            var hashed = BitConverter.ToString(hashValue).Replace("-", "");
            return hashed;
        }
    }
}
