using AutoMapper;
using EntityFramework.Exceptions.MySQL.Pomelo;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.EntityFrameworkCore;
using Quartz;
using Serilog;
using System.Text.Json.Serialization;
using Tilde.MT.WebsiteTranslationService.Extensions;
using Tilde.MT.WebsiteTranslationService.Interfaces.Providers;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Configuration;
using Tilde.MT.WebsiteTranslationService.Models.Database;
using Tilde.MT.WebsiteTranslationService.Providers;
using Tilde.MT.WebsiteTranslationService.QuartzJobs;
using Tilde.MT.WebsiteTranslationService.Services;

var builder = WebApplication.CreateBuilder(args);

var serviceConfiguration = builder.Configuration.GetSection("Services").Get<ConfigurationServices>();

builder.Services
	.AddOptions<ConfigurationServices>()
	.Bind(builder.Configuration.GetSection("Services"))
	.ValidateDataAnnotations()
	.ValidateOnStart();
builder.Services
	.AddOptions<ConfigurationSettings>()
	.Bind(builder.Configuration.GetSection("Configuration"))
	.ValidateDataAnnotations()
	.ValidateOnStart();

builder.Host
	.UseSerilog((ctx, config) =>
	{
		config
			.Enrich.FromLogContext()
			.WriteTo.Debug()
			.WriteTo.Console()
			.ReadFrom.Configuration(builder.Configuration);
	});

var mappingConfig = new MapperConfiguration(config =>
{
	config.AddProfile(new Tilde.MT.WebsiteTranslationService.Models.Mappings.MappingProfile());
});

builder.Services.AddHttpClient();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSingleton(mappingConfig.CreateMapper());
builder.Services.AddSingleton<ISegmentNormalizerProvider, SegmentNormalizerProvider>();
builder.Services.AddSingleton<UriSearchProvider>();

builder.Services.AddScoped<IMachineTranslationService, MachineTranslationService>();
builder.Services.AddScoped<ISegmentService, SegmentService>();
builder.Services.AddScoped<ITranslationService, TranslationService>();
builder.Services.AddScoped<IConfigurationService, ConfigurationService>();
builder.Services.AddScoped<IPageService, PageService>();
builder.Services.AddScoped<ExportService>();
builder.Services.AddScoped<ImportService>();
builder.Services.AddScoped<PretranslateService>();

builder.Services.AddControllers();

var configuration = builder.Configuration.GetSection("Configuration").Get<ConfigurationSettings>();

builder.Services.AddDocumentation(configuration);

builder.Services.AddHealthChecks();

#if DEBUG
builder.Services.AddCorsPolicies();
#endif
builder.Services.AddDbContextPool<ServiceDbContext>(options =>
{
	options.UseMySql(
		serviceConfiguration.Database.ConnectionString,
		ServerVersion.AutoDetect(serviceConfiguration.Database.ConnectionString)
	);
	options.UseExceptionProcessor();
});

builder.Services.AddQuartz(q =>
{
	q.UseMicrosoftDependencyInjectionJobFactory();
	q.AddJob<ImportJob>(job => job.StoreDurably());
	q.UseInMemoryStore();
});

builder.Services.AddQuartzHostedService(options =>
{
	options.WaitForJobsToComplete = true;
});

builder.Services.Configure<Microsoft.AspNetCore.Http.Json.JsonOptions>(options =>
{
	var converter = new JsonStringEnumConverter();
	options.SerializerOptions.Converters.Add(converter);
});

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
	Log.Information("Migrate database");

	var context = scope.ServiceProvider.GetRequiredService<ServiceDbContext>();
	context.Database.Migrate();

	Log.Information("Migration completed");
}

app.UseDocumentation(configuration);

app.UseRouting();

app.UseCors(builder => builder
   .AllowAnyOrigin()
   .AllowAnyMethod()
   .AllowAnyHeader()
);

#if DEBUG
app.UseCorsPolicies();
#endif

app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
	endpoints.MapControllers();

	// Startup probe / readyness probe
	endpoints.MapHealthChecks("/health/ready", new HealthCheckOptions()
	{

	});

	// Liveness 
	endpoints.MapHealthChecks("/health/live", new HealthCheckOptions()
	{

	});
});


app.Run();
