﻿using System.ComponentModel;

namespace Tilde.MT.WebsiteTranslationService.Enums.Pretranslation
{
	public enum PretranslationStatus
	{
		[Description("Ready")]
		READY = 1,

		[Description("In progress")]
		IN_PROGRESS = 2,

		[Description("Failed")]
		FAILED = 3,

		[Description("Success")]
		SUCCESS = 4
	}
}
