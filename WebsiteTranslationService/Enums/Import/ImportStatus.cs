﻿namespace Tilde.MT.WebsiteTranslationService.Enums.Import
{
	public enum ImportStatus
	{
		READY = 0,
		IN_PROGRESS = 1,
		SUCCESS = 2,
		FAIL = 3
	}
}
