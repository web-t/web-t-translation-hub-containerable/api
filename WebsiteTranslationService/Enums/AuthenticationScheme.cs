﻿namespace Tilde.MT.WebsiteTranslationService.Enums
{
    /// <summary>
    /// Authentication scheme
    /// </summary>
    public static class AuthenticationScheme
    {
        public const string KeyCloak = "keycloak";
    }
}
