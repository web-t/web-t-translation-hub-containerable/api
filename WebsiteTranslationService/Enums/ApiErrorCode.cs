﻿using System.ComponentModel;

namespace Tilde.MT.WebsiteTranslationService.Enums
{
	/// <summary>
	/// API error code, used for returning API error in the unified format, providing error code and message
	/// </summary>
	public enum ApiErrorCode
	{
		[Description("Resource not found")]
		ResourceNotFound = 0,

		[Description("Translation is not valid")]
		TranslationIsNotValid = 1,

		[Description("Cannot delete accepted translation")]
		CannotDeleteAcceptedTranslation = 2,

		[Description("Language direction not found")]
		LanguageDirectionNotFound = 3,

		[Description("Translation timeout - The response from translation provider wasn't received in time")]
		TranslationTimeout = 4,

		[Description("Domain not allowed")]
		DomainNotAllowed = 5,

		[Description("Language not found")]
		LanguageNotFound = 6,

		[Description("Translation export error")]
		CATTranslationExportError = 7,

		[Description("Error occurred while parsing translation file")]
		CATFileParsingError = 8,

		[Description("Error occurred while importing translations")]
		CATTranslationImportError = 9,

		[Description("Error occurred while starting website pretranslation")]
		PretranslationStartError = 10,

		[Description("Could not start website pretranslation due to invalid or unavailable domain and/or invalid target languages")]
		InvalidPretranslationParametersError = 11,

		[Description("Could not retrieve status of website pretranslation progress")]
		PretranslationStatusRetrieveError = 12,

		[Description("Could not reset website pretranslation status")]
		PretranslationStatusResetError = 13,

		[Description("Conflict, translation import is in progress, cannot start another import")]
		ImportAlreadyInProgressError = 14,

		[Description("Error occurred while cancelling website pretranslation")]
		PretranslationCancelError = 15,
	}
}
