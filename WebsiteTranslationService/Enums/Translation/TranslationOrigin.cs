﻿namespace Tilde.MT.WebsiteTranslationService.Enums.Translation
{
    /// <summary>
    /// Translation origin
    /// </summary>
    public enum TranslationOrigin
    {
        /// <summary>
        /// Machine translation
        /// </summary>
        MachineTranslation = 0,

        /// <summary>
        /// User suggestion
        /// </summary>
        User = 1
    }
}
