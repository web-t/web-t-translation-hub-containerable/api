﻿namespace Tilde.MT.WebsiteTranslationService.Enums.Segment
{
    /// <summary>
    /// Segment status
    /// </summary>
    public enum SegmentTranslationStatus
    {
        /// <summary>
        /// New
        /// </summary>
        New = 0,

        /// <summary>
        /// Reviewed
        /// </summary>
        Reviewed = 1
    }
}
