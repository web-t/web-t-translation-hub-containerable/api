﻿using System.Xml;
using Tilde.MT.WebsiteTranslationService.Models.Database;
using Tilde.MT.WebsiteTranslationService.Models.TranslationImport;

namespace Tilde.MT.WebsiteTranslationService.Helpers
{
	public class XmlHelper
	{
		private readonly string TempFileName = "exported_translations_temp.xliff";

		/// <summary>
		/// Creates XML file (according to XLIFF standard) of translations
		/// </summary>
		/// <param name="sourceLanguage"> Source language of website config </param>
		/// <param name="translationGroups"> Website segment translations grouped by target language </param>
		/// <returns>File as byte array</returns>
		public async Task<Byte[]> GenerateTranslationXmlFile(
			string sourceLanguage,
			Dictionary<string, List<SegmentTranslation>> translationGroups
		)
		{
			using (var writer = XmlWriter.Create(TempFileName, new XmlWriterSettings { Indent = true }))
			{
				writer.WriteStartDocument();
				writer.WriteStartElement("xliff", "urn:oasis:names:tc:xliff:document:1.2");
				writer.WriteAttributeString("version", "1.2");

				// each language direction pair has its own <file>
				foreach (var translationGroup in translationGroups)
				{
					writer.WriteStartElement("file");
					writer.WriteAttributeString("datatype", "plaintext");
					writer.WriteAttributeString("source-language", sourceLanguage);
					writer.WriteAttributeString("target-language", translationGroup.Key);
					writer.WriteAttributeString("original", "self");

					writer.WriteStartElement("body");

					foreach (SegmentTranslation translation in translationGroup.Value)
					{
						writer.WriteStartElement("trans-unit");

						if (translation.Segment.DbCreatedAt != null)
						{
							writer.WriteAttributeString("createdAt", translation.Segment.DbCreatedAt.ToString());
						}
						if (translation.Segment.DbUpdatedAt != null)
						{
							writer.WriteAttributeString("updatedAt", translation.Segment.DbUpdatedAt.ToString());
						}
						writer.WriteAttributeString("isHumanTranslation", (translation.AcceptedTranslation.Origin == Enums.Translation.TranslationOrigin.User).ToString());
						writer.WriteAttributeString("isAccepted", translation.Status == Enums.Segment.SegmentTranslationStatus.Reviewed ? "1" : "0");
						writer.WriteAttributeString("uriPath", translation.Segment.Uri);
						writer.WriteAttributeString("metadataTag", translation.Segment.SegmentMetadata.Tag);
						writer.WriteAttributeString("isSeo", translation.Segment.SegmentMetadata.IsSEO.ToString());
						if (!string.IsNullOrEmpty(translation.Segment.SegmentMetadata.Attribute))
						{
							writer.WriteAttributeString("attribute", translation.Segment.SegmentMetadata.Attribute);
						}
						if (!string.IsNullOrEmpty(translation.Segment.SegmentMetadata.ReferencedAttribute))
						{
							writer.WriteAttributeString("referencedAttribute", translation.Segment.SegmentMetadata.ReferencedAttribute);
						}

						writer.WriteStartElement("source");
						writer.WriteString(translation.Segment.TextRaw);
						writer.WriteEndElement(); // source

						writer.WriteStartElement("target");
						writer.WriteString(translation.AcceptedTranslation?.Text);
						writer.WriteEndElement(); // target

						writer.WriteEndElement(); // trans-unit
					}

					writer.WriteEndElement(); // body

					writer.WriteEndElement(); // file
				}

				writer.WriteEndElement(); // xliff

				writer.WriteEndDocument();
			}

			var bytes = await File.ReadAllBytesAsync(TempFileName);

			File.Delete(TempFileName);

			return bytes;
		}


		/// <summary>
		/// Parse XLIFF file into C# classes
		/// </summary>
		/// <returns>List of translations to save in database</returns>
		public async Task<List<TranslationForImport>> ParseXmlFile(IFormFile file)
		{
			List<TranslationForImport> translations = new List<TranslationForImport>();
			var sourceLanguage = string.Empty;
			var targetLanguage = string.Empty;

			using (var stream = file.OpenReadStream())
			{
				using (XmlReader reader = XmlReader.Create(stream))
				{
					while (reader.Read())
					{
						if (reader.NodeType == XmlNodeType.Element && reader.Name == "file")
						{
							sourceLanguage = reader.GetAttribute("source-language");
							targetLanguage = reader.GetAttribute("target-language");
						}

						if (reader.NodeType == XmlNodeType.Element && reader.Name == "trans-unit")
						{
							TranslationForImport translation = new TranslationForImport();

							translation.SourceLanguage = sourceLanguage;
							translation.TargetLanguage = targetLanguage;
							translation.CreatedTime = !string.IsNullOrEmpty(reader.GetAttribute("createdAt"))
								? DateTime.Parse(reader.GetAttribute("createdAt"))
								: DateTime.Now;
							translation.UpdatedTime = !string.IsNullOrEmpty(reader.GetAttribute("updatedAt"))
								? DateTime.Parse(reader.GetAttribute("updatedAt"))
								: DateTime.Now;
							translation.IsHumanTranslation = reader.GetAttribute("isHumanTranslation") == "True";
							translation.IsAccepted = reader.GetAttribute("isAccepted");
							translation.UriPath = reader.GetAttribute("uriPath");
							translation.MetadataTag = reader.GetAttribute("metadataTag");
							translation.IsSeo = reader.GetAttribute("isSeo") == "True";

							if (!string.IsNullOrEmpty(reader.GetAttribute("attribute")))
							{
								translation.Attribute = reader.GetAttribute("attribute");
							}
							if (!string.IsNullOrEmpty(reader.GetAttribute("referencedAttribute")))
							{
								translation.ReferencedAttribute = reader.GetAttribute("referencedAttribute");
							}

							// Read child elements - source and target texts
							while (reader.Read())
							{
								if (reader.NodeType == XmlNodeType.Element)
								{
									switch (reader.Name)
									{
										case "source":
											reader.Read(); // Move to text content
											translation.SourceText = reader.Value;
											break;
										case "target":
											reader.Read(); // Move to text content
											translation.TargetText = reader.Value;
											break;
									}
								}
								else if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "trans-unit")
								{
									break; // Exit inner loop on closing trans-unit
								}
							}

							translations.Add(translation);
						}
					}
				}
			}

			return translations;
		}
	}
}
