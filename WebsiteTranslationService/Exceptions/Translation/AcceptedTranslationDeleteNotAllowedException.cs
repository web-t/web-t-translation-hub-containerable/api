﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Translation
{
    public class AcceptedTranslationDeleteNotAllowedException : Exception
    {
        public AcceptedTranslationDeleteNotAllowedException() : base()
        {

        }
    }
}
