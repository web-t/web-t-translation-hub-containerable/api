﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Translation
{
    public class InvalidTranslationException : Exception
    {
        public InvalidTranslationException() : base("Translation is not valid")
        {

        }
    }
}
