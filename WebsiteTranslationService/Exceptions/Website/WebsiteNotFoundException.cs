﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Website
{
    public class WebsiteNotFoundException : Exception
    {
        public WebsiteNotFoundException(Guid websiteId) : base($"Website '{websiteId}' not found")
        {
        }
    }
}
