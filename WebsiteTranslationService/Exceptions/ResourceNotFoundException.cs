﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions
{
    public class ResourceNotFoundException : Exception
    {
        public ResourceNotFoundException() : base()
        {

        }
    }
}
