﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Import
{
	public class FileParsingException : Exception
	{
		public FileParsingException() : base("Could not parse translation file")
		{

		}
	}
}
