﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Import
{
	public class ImportAlreadyInProgressException : Exception
	{
		public ImportAlreadyInProgressException() : base("Translation import currently is in progress, cannot import new translations")
		{

		}
	}
}
