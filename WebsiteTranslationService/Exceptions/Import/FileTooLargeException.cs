﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Import
{
	public class FileTooLargeException : Exception
	{
		public FileTooLargeException(string maxFileSizeMB) : base($"File size exceeded max. allowed limit ({maxFileSizeMB} MB)!")
		{

		}
	}
}
