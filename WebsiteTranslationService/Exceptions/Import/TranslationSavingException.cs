﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Import
{
	public class TranslationSavingException : Exception
	{
		public TranslationSavingException() : base("An issue occured while saving imported translations")
		{

		}
	}
}
