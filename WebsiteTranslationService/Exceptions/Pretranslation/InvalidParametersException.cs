﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Pretranslation
{
	public class InvalidParametersException : Exception
	{
		public InvalidParametersException(string message) : base(message)
		{

		}
	}
}
