﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Pretranslation
{
	public class ConfigurationNotFoundException : Exception
	{
		public ConfigurationNotFoundException(Guid configurationId) : base($"Configuration with ID {configurationId} was not found")
		{

		}
	}
}
