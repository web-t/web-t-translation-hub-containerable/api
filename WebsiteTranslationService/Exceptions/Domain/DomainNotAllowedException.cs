﻿namespace Tilde.MT.WebsiteTranslationService.Exceptions.Domain
{
    public class DomainNotAllowedException : Exception
    {
        public DomainNotAllowedException(Guid websiteId, Uri? referer) : base($"Domain '{referer?.DnsSafeHost}' not allowed for website '{websiteId}'")
        {

        }
    }
}
