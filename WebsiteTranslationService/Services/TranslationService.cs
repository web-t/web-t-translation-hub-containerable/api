﻿using Microsoft.EntityFrameworkCore;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.MT.WebsiteTranslationService.Enums.Segment;
using Tilde.MT.WebsiteTranslationService.Exceptions.Domain;
using Tilde.MT.WebsiteTranslationService.Interfaces.Providers;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Database;

namespace Tilde.MT.WebsiteTranslationService.Services
{
    public class TranslationService : ITranslationService
    {
        public record SegmentUniqueIdWithoutUri
        {
            public string TextId { get; set; }
            public bool IsSEO { get; init; }
            public string Tag { get; init; }
            public string Attribute { get; init; }
            public string ReferencedAttribute { get; init; }
        }
        private record RequestSegment
        {
            public SegmentUniqueIdWithoutUri SegmentId { get; set; }
            public Models.DTO.Translation.SegmentRequestText Raw { get; set; }
        }

        private record TranslationSegmentMetadataKey
        {
            public bool IsSEO { get; init; }
            public string Tag { get; init; }
            public string? Attribute { get; init; }
            public string? ReferencedAttribute { get; init; }
        }

        private bool segmentsLoaded = false;
        private readonly ILogger _logger;
        private readonly ServiceDbContext _dbContext;
        private readonly IMachineTranslationService _translationService;
        private readonly ISegmentNormalizerProvider _segmentNormalizer;
        private readonly IConfigurationService _configurationService;
        private Dictionary<SegmentUniqueIdWithoutUri, long> segments;
        private HashSet<string> segmentsToSearchInCache;

        public TranslationService(
            ILogger<TranslationService> logger,
            ServiceDbContext dbContext,
            IMachineTranslationService translationService,
            ISegmentNormalizerProvider segmentNormalizer,
            IConfigurationService configurationService
        )
        {
            _logger = logger;
            _dbContext = dbContext;
            _translationService = translationService;
            _segmentNormalizer = segmentNormalizer;
            _configurationService = configurationService;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Models.DTO.Translation.TranslationItem>> Translate(Guid websiteId, Models.DTO.Translation.SegmentRequest request, Uri? referer)
        {
            var response = new List<Models.DTO.Translation.TranslationItem>(request.Text.Count);
            var uriId = _segmentNormalizer.Normalize(request.Uri);

            var requestedSegments = request.Text.Select(item => new RequestSegment
            {
                Raw = item,
                SegmentId = new SegmentUniqueIdWithoutUri
                {
                    TextId = _segmentNormalizer.Normalize(item.Text),
                    Attribute = item.Metadata.Attribute,
                    IsSEO = item.Metadata.IsSEO,
                    Tag = item.Metadata.Tag,
                    ReferencedAttribute = item.Metadata.ReferencedAttribute
                }
            });

            segmentsToSearchInCache = requestedSegments
               .Select(item => item.SegmentId.TextId)
               .ToHashSet();

            var configuration = await _configurationService.Get(websiteId);

            if (configuration.SiteUrls.Any())
            {
                if (referer == null || !configuration.SiteUrls.Select(x => new Uri(x).DnsSafeHost).Contains(referer.DnsSafeHost))
                {
                    throw new DomainNotAllowedException(websiteId, referer);
                }
            }

            var languageDirection = configuration.Languages
                .Where(x => x.TargetLanguage == request.TargetLanguage)
                .FirstOrDefault();

            if (languageDirection == null)
            {
                throw new LanguageDirectionNotFoundException(request.TargetLanguage);
            }

            var cachedSegmentTranslations = await _dbContext.SegmentTranslations
                .AsNoTracking()
                .Where(item => item.Segment.WebsiteId == websiteId)
                .Where(item => item.Segment.UriId == uriId)
                .Where(item => segmentsToSearchInCache.Contains(item.Segment.TextNormalized))
                .Where(item => item.WebsiteTargetLanguageName == request.TargetLanguage)
                .Select(item => new
                {
                    TextId = item.Segment.TextNormalized,
                    SegmentId = item.Segment.Id,
                    AcceptedTranslation = item.AcceptedTranslation,
                    Metadata = item.Segment.SegmentMetadata
                })
                .ToDictionaryAsync(item => new SegmentUniqueIdWithoutUri
                {
                    TextId = item.TextId,
                    Attribute = item.Metadata.Attribute!,
                    IsSEO = item.Metadata.IsSEO,
                    Tag = item.Metadata.Tag,
                    ReferencedAttribute = item.Metadata.ReferencedAttribute!
                },
            item => item);

            var cachedTranslationMetadata = new Dictionary<TranslationSegmentMetadataKey, SegmentMetadata>();

            var createdSegmentTranslations = new Dictionary<SegmentUniqueIdWithoutUri, Models.DTO.Translation.TranslationItem>(Math.Max(request.Text.Count, cachedSegmentTranslations.Count));

            var textsToTranslateWithMT = requestedSegments
                .Where(item =>
                {
                    if (cachedSegmentTranslations.TryGetValue(item.SegmentId, out var cachedSegmentTranslation) && cachedSegmentTranslation.AcceptedTranslation != null)
                    {
                        return false;
                    }
                    return true;
                })
                .GroupBy(item => item.Raw.Text);

            IEnumerable<string> rawMtTranslations = new List<string>();
            if (textsToTranslateWithMT.Any())
            {
                rawMtTranslations = await _translationService.Translate(configuration.SourceLanguage, languageDirection.TargetLanguage, languageDirection.Domain, textsToTranslateWithMT.Select(item => item.Key));
            }
            var mtTranslations = textsToTranslateWithMT.Zip(rawMtTranslations)
                .ToDictionary(item => item.First.Key, item => item.Second);

            foreach (var text in requestedSegments)
            {
                if (cachedSegmentTranslations.TryGetValue(text.SegmentId, out var cachedSegmentTranslation))
                {
                    if (cachedSegmentTranslation.AcceptedTranslation != null)
                    {
                        response.Add(new Models.DTO.Translation.TranslationItem()
                        {
                            SourceSegmentId = cachedSegmentTranslation.SegmentId,
                            Translation = cachedSegmentTranslation.AcceptedTranslation.Text
                        });
                        continue;
                    }
                }

                if (createdSegmentTranslations.TryGetValue(text.SegmentId, out Models.DTO.Translation.TranslationItem? textResponse))
                {
                    response.Add(textResponse);
                    continue;
                }

                var mtTranslation = mtTranslations[text.Raw.Text];
                var mtTextResponse = new Models.DTO.Translation.TranslationItem()
                {
                    Translation = mtTranslation!
                };

                createdSegmentTranslations.Add(text.SegmentId, mtTextResponse);
                response.Add(mtTextResponse);

                await LazyLoadSegments(websiteId, request.Uri);

                await SaveMTTranslation(websiteId, text, mtTranslation!, languageDirection.TargetLanguage, request.Uri, cachedTranslationMetadata);
            }

            await _dbContext.SaveChangesAsync();

            return response;
        }

        /// <summary>
        /// Load segments for a website
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="requestUri"> Request URI </param>
        /// <returns></returns>
        async Task LazyLoadSegments(Guid websiteId, string requestUri)
        {
            if (segmentsLoaded)
            {
                return;
            }

            segmentsLoaded = true;

            segments = await _dbContext.Segments
                .AsNoTracking()
                .Where(item => item.WebsiteId == websiteId)
                .Where(item => segmentsToSearchInCache.Contains(item.TextNormalized))
                .Where(item => item.UriId == _segmentNormalizer.Normalize(requestUri))
                .Select(item => new
                {
                    TextId = item.TextNormalized,
                    Metadata = item.SegmentMetadata,
                    Id = item.Id
                })
                .ToDictionaryAsync(item => new SegmentUniqueIdWithoutUri
                {
                    TextId = item.TextId,
                    Attribute = item.Metadata.Attribute,
                    IsSEO = item.Metadata.IsSEO,
                    Tag = item.Metadata.Tag,
                    ReferencedAttribute = item.Metadata.ReferencedAttribute
                },
                item => item.Id);
        }

        /// <summary>
        /// Save MT translation for a website
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="text"> Reqeust segment </param>
        /// <param name="mtTranslation"> MT translation text </param>
        /// <param name="languageDirectionName"> Website target language name </param>
        /// <param name="requestUri"> Request URI </param>
        /// <param name="cachedTranslationMetadata"> Cached translation metadata </param>
        /// <returns></returns>
        private async Task SaveMTTranslation(
            Guid websiteId,
            RequestSegment text,
            string mtTranslation,
            string languageDirectionName,
            string requestUri,
            Dictionary<TranslationSegmentMetadataKey, SegmentMetadata> cachedTranslationMetadata
        )
        {
            var translation = new Translation()
            {
                Text = mtTranslation,
                DbCreatedAt = DateTime.UtcNow,
                DbUpdatedAt = DateTime.UtcNow
            };

            SegmentTranslation segmentTranslation;

            if (segments!.TryGetValue(text.SegmentId, out long SegmentId))
            {
                segmentTranslation = new SegmentTranslation()
                {
                    WebsiteTargetLanguageName = languageDirectionName,
                    WebsiteId = websiteId,
                    AcceptedTranslation = translation,
                    Status = SegmentTranslationStatus.New,
                    SegmentId = SegmentId,
                    DbCreatedAt = DateTime.UtcNow,
                    DbUpdatedAt = DateTime.UtcNow
                };
            }
            else
            {
                var translationMetadataKey = new TranslationSegmentMetadataKey()
                {
                    Attribute = text.SegmentId.Attribute,
                    IsSEO = text.SegmentId.IsSEO,
                    ReferencedAttribute = text.SegmentId.ReferencedAttribute,
                    Tag = text.SegmentId.Tag
                };
                SegmentMetadata? segmentMetadata;

                if (!cachedTranslationMetadata.TryGetValue(translationMetadataKey, out segmentMetadata))
                {
                    segmentMetadata = await _dbContext.SegmentMetadata
                        .Where(item => item.IsSEO == text.SegmentId.IsSEO)
                        .Where(item => item.Attribute == text.SegmentId.Attribute)
                        .Where(item => item.ReferencedAttribute == text.SegmentId.ReferencedAttribute)
                        .Where(item => item.Tag == text.SegmentId.Tag)
                        .FirstOrDefaultAsync();

                    if (segmentMetadata == null)
                    {
                        segmentMetadata = new SegmentMetadata()
                        {
                            Tag = text.SegmentId.Tag,
                            Attribute = text.SegmentId.Attribute,
                            ReferencedAttribute = text.SegmentId.ReferencedAttribute,
                            IsSEO = text.SegmentId.IsSEO
                        };

                        _dbContext.SegmentMetadata.Add(segmentMetadata);
                    }

                    cachedTranslationMetadata[translationMetadataKey] = segmentMetadata;
                }
                Segment segment = new()
                {
                    Uri = requestUri,
                    UriId = _segmentNormalizer.Normalize(requestUri),
                    TextNormalized = text.SegmentId.TextId,
                    TextRaw = text.Raw.Text,
                    SegmentMetadata = segmentMetadata,
                    WebsiteId = websiteId,
                    DbCreatedAt = DateTime.UtcNow,
                    DbUpdatedAt = DateTime.UtcNow
                };

                _dbContext.Segments.Add(segment);

                segmentTranslation = new SegmentTranslation()
                {
                    WebsiteTargetLanguageName = languageDirectionName,
                    WebsiteId = websiteId,
                    AcceptedTranslation = translation,
                    Status = SegmentTranslationStatus.New,
                    Segment = segment,
                    DbCreatedAt = DateTime.UtcNow,
                    DbUpdatedAt = DateTime.UtcNow,
                };
            }

            var segmentTranslationHistory = new SegmentTranslationHistory()
            {
                SegmentTranslation = segmentTranslation,
                Translation = translation
            };

            _dbContext.Translations.Add(translation);

            _dbContext.SegmentTranslations.Add(segmentTranslation);
            _dbContext.SegmentTranslationHistories.Add(segmentTranslationHistory);
        }
    }
}
