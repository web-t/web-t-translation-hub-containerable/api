﻿using AutoMapper;
using Microsoft.Extensions.Options;
using System.Text.Json;
using Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Configuration;

namespace Tilde.MT.WebsiteTranslationService.Services
{
	public class ConfigurationService : IConfigurationService
	{
		private readonly ILogger _logger;
		private readonly ConfigurationServices _serviceConfiguration;
		private readonly IHttpClientFactory _httpClientFactory;

		public ConfigurationService(
			ILogger<ConfigurationService> logger,
			IMapper mapper,
			IOptions<ConfigurationServices> serviceConfiguration,
			IHttpClientFactory httpClientFactory
		)
		{
			_logger = logger;
			_serviceConfiguration = serviceConfiguration.Value;
			_httpClientFactory = httpClientFactory;
		}

		/// <inheritdoc/>
		public async Task<Configuration> Get(Guid id)
		{
			var client = _httpClientFactory.CreateClient();

			var result = await client.GetAsync($"{_serviceConfiguration.Configuration.Uri}/configuration/{id}");

			result.EnsureSuccessStatusCode();

			var json = await result.Content.ReadAsStringAsync();

			var configuration = JsonSerializer.Deserialize<Configuration>(json);

			return configuration;
		}
	}
}
