﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Quartz;
using System.Text.Json;
using Tilde.MT.WebsiteTranslationService.Enums.Import;
using Tilde.MT.WebsiteTranslationService.Exceptions.Import;
using Tilde.MT.WebsiteTranslationService.Helpers;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Configuration;
using Tilde.MT.WebsiteTranslationService.Models.Database;
using Tilde.MT.WebsiteTranslationService.Models.TranslationImport;
using Tilde.MT.WebsiteTranslationService.QuartzJobs;

namespace Tilde.MT.WebsiteTranslationService.Services
{
	public class ImportService
	{
		private readonly IConfigurationService _configurationService;
		private readonly IOptions<ConfigurationSettings> _configurationSettings;
		private readonly ILogger _logger;
		private readonly ISchedulerFactory _factory;
		private readonly ServiceDbContext _dbContext;

		private readonly XmlHelper _xmlHelper;

		public ImportService(
			IConfigurationService configurationService,
			IOptions<ConfigurationSettings> configurationSettings,
			ILogger<ImportService> logger,
			ISchedulerFactory factory,
			ServiceDbContext dbContext
		)
		{
			_configurationService = configurationService;
			_configurationSettings = configurationSettings;
			_logger = logger;
			_factory = factory;
			_dbContext = dbContext;

			_xmlHelper = new XmlHelper();
		}

		public async Task ImportTranslations(IFormFile xliffFile, Guid configId)
		{
			var configuration = await _configurationService.Get(configId);
			var translationsForImport = new List<TranslationForImport>();

			if (xliffFile.Length > _configurationSettings.Value.MaxImportFileSize)
			{
				var fileSizeInMB = _configurationSettings.Value.MaxImportFileSize / (1024.0 * 1024.0);

				throw new FileTooLargeException(fileSizeInMB.ToString());
			}

			try
			{
				translationsForImport = await _xmlHelper.ParseXmlFile(xliffFile);
			}
			catch (Exception)
			{
				throw new FileParsingException();
			}

			var currentStatus = await GetImportStatusAsync(configId);

			if (currentStatus == ImportStatus.IN_PROGRESS)
			{
				throw new ImportAlreadyInProgressException();
			}

			if (translationsForImport.Any())
			{
				IScheduler scheduler = await _factory.GetScheduler();

				IJobDetail importJob = JobBuilder
					.Create<ImportJob>()
					.UsingJobData("configuration", JsonSerializer.Serialize(configuration))
					.UsingJobData("translationsForImport", JsonSerializer.Serialize(translationsForImport))
					.Build();

				var trigger = TriggerBuilder
					.Create()
					.StartNow()
					.Build();
				await scheduler.ScheduleJob(importJob, trigger);

				await CreateOrUpdateImportStatusAsync(configuration.Id, ImportStatus.IN_PROGRESS);
			}
		}

		public async Task<ImportStatus> GetImportStatusAsync(Guid configId)
		{
			var existingStatus = await _dbContext.TranslationImportStatuses.FirstOrDefaultAsync(item => item.ConfigurationId == configId);

			if (existingStatus != null)
			{
				return existingStatus.ImportStatus;
			}
			return ImportStatus.READY;
		}

		public async Task CreateOrUpdateImportStatusAsync(Guid configId, ImportStatus importStatus)
		{
			var existingStatus = await _dbContext.TranslationImportStatuses.FirstOrDefaultAsync(item => item.ConfigurationId == configId);

			if (existingStatus != null)
			{
				existingStatus.ImportStatus = importStatus;
			}
			else
			{
				await _dbContext.TranslationImportStatuses.AddAsync(new TranslationImportStatus()
				{
					ConfigurationId = configId,
					ImportStatus = importStatus
				});
			}

			await _dbContext.SaveChangesAsync();
		}
	}
}
