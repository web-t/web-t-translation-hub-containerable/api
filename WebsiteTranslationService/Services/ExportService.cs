﻿using Microsoft.EntityFrameworkCore;
using Tilde.MT.WebsiteTranslationService.Helpers;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Database;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Translation;

namespace Tilde.MT.WebsiteTranslationService.Services
{
	public class ExportService
	{
		private readonly ServiceDbContext _dbContext;
		private readonly IConfigurationService _configurationService;
		private readonly XmlHelper _xmlHelper;

		public ExportService(ServiceDbContext dbContext, IConfigurationService configurationService)
		{
			_dbContext = dbContext;
			_configurationService = configurationService;

			_xmlHelper = new XmlHelper();
		}

		public async Task<List<SegmentTranslation>> ListSegments(Guid websiteId, SegmentListFilterForExport filters)
		{
			var query = _dbContext.SegmentTranslations
				.Where(item => item.Segment.WebsiteId == websiteId);

			if (filters.TargetLanguages == null)
			{
				filters.TargetLanguages = Array.Empty<string>();
			}

			query = query.Where(item => filters.TargetLanguages.Contains(item.WebsiteTargetLanguageName));

			if (filters.Origin.HasValue)
			{
				query = query.Where(item => item.AcceptedTranslation.Origin == filters.Origin);
			}
			if (filters.Status.HasValue)
			{
				query = query.Where(item => item.Status == filters.Status);
			}
			if (filters.IsSEO != null)
			{
				query = query.Where(item => item.Segment.SegmentMetadata.IsSEO == filters.IsSEO);
			}

			var segments = await query
				.Include(item => item.AcceptedTranslation)
				.Include(item => item.Segment)
					.ThenInclude(item => item.SegmentMetadata)
				.OrderByDescending(item => item.Id)
				.ToListAsync();

			return segments;
		}

		public async Task<Byte[]> ExportWebsiteTranslations(Guid configId, SegmentListFilterForExport filters)
		{
			var configuration = await _configurationService.Get(configId);

			var segments = await ListSegments(configId, filters);

			var segmentsGroupedByTargetLanguage = segments
				.GroupBy(segment => segment.WebsiteTargetLanguageName)
				.ToDictionary(g => g.Key, g => g.ToList());

			var fileContent = await _xmlHelper.GenerateTranslationXmlFile(configuration.SourceLanguage, segmentsGroupedByTargetLanguage);

			return fileContent;
		}
	}
}
