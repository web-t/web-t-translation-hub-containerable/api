﻿using Microsoft.Extensions.Options;
using System.Text.Json;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.EMW.Contracts.Models.Common.Translation;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Configuration;

namespace Tilde.MT.WebsiteTranslationService.Services
{
    public class MachineTranslationService : IMachineTranslationService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ConfigurationServices _serviceConfiguration;

        public MachineTranslationService(
            IOptions<ConfigurationServices> serviceConfiguration,
            IHttpClientFactory httpClientFactory
        )
        {
            _serviceConfiguration = serviceConfiguration.Value;
            _httpClientFactory = httpClientFactory;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<string>> Translate(string sourceLanguage, string targetLanguage, string domain, IEnumerable<string> text)
        {
            var client = _httpClientFactory.CreateClient();

            var uri = new Uri($"{_serviceConfiguration.TextTranslation.Uri}/translate/text");
            var content = new TranslationRequest()
            {
                SourceLanguage = sourceLanguage,
                TargetLanguage = targetLanguage,
                Domain = domain,
                Text = text.ToList()
            };

            var response = await client.PostAsJsonAsync(uri, content);

            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new LanguageDirectionNotFoundException(uri.DnsSafeHost, sourceLanguage, targetLanguage);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.GatewayTimeout)
            {
                throw new TranslationTimeoutException();
            }

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();

            var json = JsonSerializer.Deserialize<Translation>(jsonResponse);

            return json.Translations.Select(item => item.Translation);
        }
    }
}
