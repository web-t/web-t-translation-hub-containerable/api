﻿using Microsoft.Extensions.Options;
using Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;
using Tilde.MT.WebsiteTranslationService.Exceptions.Pretranslation;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Configuration;
using Tilde.MT.WebsiteTranslationService.Models.Pretranslation;

namespace Tilde.MT.WebsiteTranslationService.Services
{
	public class PretranslateService
	{
		private readonly ILogger _logger;
		private readonly IHttpClientFactory _httpClientFactory;
		private readonly IConfigurationService _configurationService;

		private readonly ConfigurationServices _serviceConfiguration;

		public PretranslateService(
			ILogger<PretranslateService> logger,
			IHttpClientFactory httpClientFactory,
			IOptions<ConfigurationServices> serviceConfiguration,
			IConfigurationService configurationService)
		{
			_logger = logger;
			_httpClientFactory = httpClientFactory;
			_configurationService = configurationService;

			_serviceConfiguration = serviceConfiguration.Value;
		}

		public async Task StartWebsitePretranslation(PretranslationRequest pretranslationRequest)
		{
			var configuration = await _configurationService.Get(pretranslationRequest.ConfigurationId);

			if (configuration == null)
			{
				throw new ConfigurationNotFoundException(pretranslationRequest.ConfigurationId);
			}

			await CheckRequestedParameters(configuration, pretranslationRequest.Domain, pretranslationRequest.TargetLanguages);

			var client = _httpClientFactory.CreateClient();

			var uri = new Uri($"{_serviceConfiguration.CrawlerAgent.Uri}/pretranslate-website");
			HttpContent content = new FormUrlEncodedContent(new Dictionary<string, string>
				{
						{ "configurationId", pretranslationRequest.ConfigurationId.ToString() },
						{ "websiteDomain", pretranslationRequest.Domain },
						{ "targetLanguages", string.Join(",", pretranslationRequest.TargetLanguages) },
					}
			);

			var response = await client.PostAsync(uri, content);

			response.EnsureSuccessStatusCode();
		}

		public async Task StartWebsitePretranslationRetry(Guid websiteId)
		{
			var configuration = await _configurationService.Get(websiteId);

			if (configuration == null)
			{
				throw new ConfigurationNotFoundException(websiteId);
			}

			var client = _httpClientFactory.CreateClient();


			var uri = new Uri($"{_serviceConfiguration.CrawlerAgent.Uri}/retry-pretranslation");
			HttpContent content = new FormUrlEncodedContent(new Dictionary<string, string>
				{
					{ "configurationId", websiteId.ToString() },
				}
			);

			var response = await client.PostAsync(uri, content);

			response.EnsureSuccessStatusCode();
		}

		public async Task ResetWebsitePretranslationStatus(Guid websiteId)
		{
			var configuration = await _configurationService.Get(websiteId);

			if (configuration == null)
			{
				throw new ConfigurationNotFoundException(websiteId);
			}

			var client = _httpClientFactory.CreateClient();


			var uri = new Uri($"{_serviceConfiguration.CrawlerAgent.Uri}/reset-pretranslation-status");
			HttpContent content = new FormUrlEncodedContent(new Dictionary<string, string>
				{
					{ "configurationId", websiteId.ToString() },
				}
			);

			var response = await client.PostAsync(uri, content);

			response.EnsureSuccessStatusCode();
		}

		public async Task<PretranslationStatusResponse> GetPretranslationStatus(Guid websiteId)
		{
			var configuration = await _configurationService.Get(websiteId);

			if (configuration == null)
			{
				throw new ConfigurationNotFoundException(websiteId);
			}

			var client = _httpClientFactory.CreateClient();

			var uri = new Uri($"{_serviceConfiguration.CrawlerAgent.Uri}/pretranslation-status?configurationId={configuration.Id}");

			var response = await client.GetAsync(uri);

			response.EnsureSuccessStatusCode();

			var statusResponse = response.Content.ReadFromJsonAsync<PretranslationStatusResponse>().Result;

			return statusResponse;
		}

		private async Task CheckRequestedParameters(Configuration configuration, string domain, List<string> targetLanguages)
		{
			var domainIsAllowed = configuration.SiteUrls.Any(url => url.Equals(domain, StringComparison.OrdinalIgnoreCase));

			if (!domainIsAllowed)
			{
				throw new InvalidParametersException($"Passed domain {domain} is not allowed");
			}

			var checkResponse = await CheckWebsiteAvailability(domain);
			if (checkResponse == null || !checkResponse.IsSuccessStatusCode)
			{
				throw new InvalidParametersException($"Domain {domain} is not publicly available and cannot be pretranslated, availability check response status code: {checkResponse?.StatusCode}");
			}

			targetLanguages = targetLanguages.ConvertAll(l => l.ToLower());
			var configTargetLangs = configuration.Languages.Select(l => l.TargetLanguage.ToLower()).ToList();

			var allTargetLanguagesAreValid = targetLanguages.TrueForAll(l => configTargetLangs.Contains(l));
			if (!allTargetLanguagesAreValid)
			{
				throw new InvalidParametersException($"Passed target languages {string.Join(", ", targetLanguages)} are not valid");
			}
		}

		private async Task<HttpResponseMessage?> CheckWebsiteAvailability(string domain)
		{
			using (HttpClient client = new HttpClient())
			{
				var response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, domain));

				return response;
			}
		}

		public async Task CancelWebsitePretranslation(Guid websiteId)
		{
			var configuration = await _configurationService.Get(websiteId);

			if (configuration == null)
			{
				throw new ConfigurationNotFoundException(websiteId);
			}

			var client = _httpClientFactory.CreateClient();


			var uri = new Uri($"{_serviceConfiguration.CrawlerAgent.Uri}/cancel-pretranslation");
			HttpContent content = new FormUrlEncodedContent(new Dictionary<string, string>
				{
					{ "configurationId", websiteId.ToString() },
				}
			);

			var response = await client.PostAsync(uri, content);

			response.EnsureSuccessStatusCode();
		}

	}
}
