﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Tilde.MT.TagTranslationValidator.Validators;
using Tilde.MT.WebsiteTranslationService.Enums.Translation;
using Tilde.MT.WebsiteTranslationService.Exceptions;
using Tilde.MT.WebsiteTranslationService.Exceptions.Translation;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Database;
using Tilde.MT.WebsiteTranslationService.Models.DTO;
using Tilde.MT.WebsiteTranslationService.Providers;

namespace Tilde.MT.WebsiteTranslationService.Services
{
    public class SegmentService : ISegmentService
    {
        private readonly ILogger _logger;
        private readonly ServiceDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly UriSearchProvider _uriSearchProvider;
        private readonly IConfigurationService _configurationService;

        public SegmentService(
            ILogger<SegmentService> logger,
            ServiceDbContext dbContext,
            IMapper mapper,
            UriSearchProvider uriSearchProvider,
            IConfigurationService configurationService
        )
        {
            _logger = logger;
            _dbContext = dbContext;
            _mapper = mapper;
            _uriSearchProvider = uriSearchProvider;
            _configurationService = configurationService;
        }

        /// <inheritdoc/>
        public async Task<SegmentList> ListSegments(ClaimsPrincipal user, Guid websiteId, Models.DTO.Translation.SegmentListFilter filters)
        {
            var result = new SegmentList()
            {
                TotalSegments = 0,
                Segments = new List<Models.DTO.Translation.Segment>()
            };
            var domainSearchAllowed = true;

            var query = _dbContext.SegmentTranslations
                .Where(item => item.Segment.WebsiteId == websiteId);

            if (filters.Language != null)
            {
                query = query.Where(item => item.WebsiteTargetLanguageName == filters.Language);
            }
            if (filters.Text != null)
            {
                query = query.Where(item => item.Segment.TextRaw.Contains(filters.Text) || (item.AcceptedTranslation != null && item.AcceptedTranslation.Text.Contains(filters.Text)));
            }
            if (filters.Uri != null)
            {
                var availableDomains = await _configurationService.Get(websiteId);

                domainSearchAllowed = _uriSearchProvider.CanSearch(filters.Uri, availableDomains.SiteUrls, out string? normalizedUri);

                query = query.Where(item => item.Segment.Uri.StartsWith(normalizedUri));
            }
            if (filters.Origin.HasValue)
            {
                query = query.Where(item => item.AcceptedTranslation.Origin == filters.Origin);
            }
            if (filters.Status.HasValue)
            {
                query = query.Where(item => item.Status == filters.Status);
            }
            if (filters.IsSEO != null)
            {
                query = query.Where(item => item.Segment.SegmentMetadata.IsSEO == filters.IsSEO);
            }

            if (domainSearchAllowed)
            {
                var segmentStats = await query.CountAsync();

                var segments = await query
                    .Include(item => item.AcceptedTranslation)
                    .Include(item => item.Segment)
                        .ThenInclude(item => item.SegmentMetadata)
                    .OrderByDescending(item => item.Id) // Probably the same ordering as createdAt
                    .Skip(filters.From)
                    .Take(filters.Count)
                    .ToListAsync();

                result.TotalSegments = segmentStats;
                result.Segments = segments.Select(item => _mapper.Map<Models.DTO.Translation.Segment>(item.Segment));
            }
            return result;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Models.DTO.Translation.Translation>> ListTranslations(ClaimsPrincipal user, Guid websiteId, long segmentId, string targetLanguage)
        {
            var translations = await _dbContext.SegmentTranslationHistories
                .Where(item => item.SegmentTranslation.Segment.Id == segmentId)
                .Where(item => item.SegmentTranslation.Segment.WebsiteId == websiteId)
                .Where(item => item.SegmentTranslation.WebsiteTargetLanguageName == targetLanguage)
                .Include(item => item.Translation)
                .Include(item => item.SegmentTranslation)
                .ToListAsync();

            return translations.Select(item => _mapper.Map<Models.DTO.Translation.Translation>(item.Translation));
        }

        /// <inheritdoc/>
        public async Task<Models.DTO.Translation.Translation> Add(ClaimsPrincipal user, Guid websiteId, long segmentId, string targetLanguage, string text, string uri)
        {
            var configuration = await _configurationService.Get(websiteId);

            var targetLang = configuration.Languages
                .Where(x => x.TargetLanguage == targetLanguage)
                .FirstOrDefault();

            if (targetLang == null)
            {
                throw new ResourceNotFoundException();
            }

            var segment = await _dbContext.Segments
                .Where(item => item.WebsiteId == websiteId)
                .Where(item => item.Id == segmentId)
                .FirstOrDefaultAsync();

            if (segment == null)
            {
                throw new ResourceNotFoundException();
            }

            var validator = new SegmentValidator();
            if (validator.Validate(segment.TextRaw, text) == false)
            {
                throw new InvalidTranslationException();
            }

            var segmentTranslation = await _dbContext.SegmentTranslations
                .Where(item => item.Segment.Id == segment.Id)
                .Where(item => item.Segment.WebsiteId == websiteId)
                .Where(item => item.WebsiteTargetLanguageName == targetLanguage)
                .FirstOrDefaultAsync();

            if (segmentTranslation == null)
            {
                segmentTranslation = new SegmentTranslation()
                {
                    AcceptedTranslation = null,
                    Segment = segment,
                    Status = Enums.Segment.SegmentTranslationStatus.New,
                    WebsiteTargetLanguageName = targetLang.TargetLanguage,
                    WebsiteId = websiteId,
                    DbCreatedAt = DateTime.UtcNow,
                    DbUpdatedAt = DateTime.UtcNow
                };

                _dbContext.SegmentTranslations.Add(segmentTranslation);
            }

            var translation = new Translation()
            {
                Origin = TranslationOrigin.User,
                Text = text,
                DbCreatedAt = DateTime.UtcNow,
                DbUpdatedAt = DateTime.UtcNow
            };

            _dbContext.Translations.Add(translation);

            segmentTranslation.AcceptedTranslation = translation;
            segmentTranslation.Status = Enums.Segment.SegmentTranslationStatus.New;

            _dbContext.SegmentTranslationHistories.Add(new SegmentTranslationHistory()
            {
                SegmentTranslation = segmentTranslation,
                Translation = translation
            });

            await _dbContext.SaveChangesAsync();

            var result = _mapper.Map<Models.DTO.Translation.Translation>(translation);
            result.Language = targetLanguage;

            return result;
        }

        /// <inheritdoc/>
        public async Task Accept(ClaimsPrincipal user, Guid websiteId, string lang, long segmentId, long translationId)
        {
            var segmentTranslation = await _dbContext.SegmentTranslations
                .Where(item => item.WebsiteId == websiteId)
                .Where(item => item.SegmentId == segmentId)
                .Where(item => item.WebsiteTargetLanguageName == lang)
                .FirstOrDefaultAsync();

            if (segmentTranslation == null)
            {
                throw new ResourceNotFoundException();
            }

            var translationToAccept = await _dbContext.SegmentTranslationHistories
                .Where(item => item.TranslationId == translationId)
                .Where(item => item.SegmentTranslation.Segment.Id == segmentId)
                .Where(item => item.SegmentTranslation.WebsiteId == websiteId)
                .Where(item => item.SegmentTranslation.WebsiteTargetLanguageName == lang)
                .Include(item => item.Translation)
                .FirstOrDefaultAsync();

            segmentTranslation.AcceptedTranslation = translationToAccept.Translation;
            segmentTranslation.Status = Enums.Segment.SegmentTranslationStatus.Reviewed;
            segmentTranslation.DbUpdatedAt = DateTime.UtcNow;

            await _dbContext.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task Remove(ClaimsPrincipal user, Guid websiteId, long sourceSegmentId, string language, long suggestionId)
        {
            var translationHistory = await _dbContext.SegmentTranslationHistories
                .Where(item => item.TranslationId == suggestionId)
                .Where(item => item.SegmentTranslationId == sourceSegmentId)
                .Where(item => item.SegmentTranslation.WebsiteId == websiteId)
                .Include(item => item.Translation)
                .Include(item => item.SegmentTranslation)
                    .ThenInclude(item => item.AcceptedTranslation)
                .FirstOrDefaultAsync();

            if (translationHistory == null)
            {
                throw new ResourceNotFoundException();
            }

            if (translationHistory.Id == translationHistory.SegmentTranslation.AcceptedTranslation.Id)
            {
                throw new AcceptedTranslationDeleteNotAllowedException();
            }

            _dbContext.SegmentTranslationHistories.Remove(translationHistory);
            _dbContext.Translations.Remove(translationHistory.Translation);

            await _dbContext.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task DeleteSegment(ClaimsPrincipal user, Guid websiteId, long segmentId)
        {
            var segment = await _dbContext.Segments
                .Where(item => item.WebsiteId == websiteId)
                .Where(item => item.Id == segmentId)
                .FirstOrDefaultAsync();

            if (segment == null)
            {
                throw new ResourceNotFoundException();
            }

            _dbContext.Segments.Remove(segment);

            await _dbContext.SaveChangesAsync();
        }
    }
}
