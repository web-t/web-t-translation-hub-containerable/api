﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Database;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Page;

namespace Tilde.MT.WebsiteTranslationService.Services
{
    public class PageService : IPageService
    {
        private readonly ILogger _logger;
        private readonly ServiceDbContext _dbContext;
        private readonly IMapper _mapper;

        public PageService(
            ILogger<SegmentService> logger,
            ServiceDbContext dbContext,
            IMapper mapper
        )
        {
            _logger = logger;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<PageList> ListPages(ClaimsPrincipal user, Guid websiteId, Models.DTO.Page.PageListFilter filters)
        {
            var result = new PageList()
            {
                TotalPages = 0,
                Pages = new List<Models.DTO.Page.Page>()
            };

            var filterQuery = _dbContext.Segments
                .Where(item => item.WebsiteId == websiteId);

            if (!string.IsNullOrEmpty(filters.Text))
            {
                filterQuery = filterQuery
                    .Where(item => item.Uri.Contains(filters.Text));
            }

            var query = filterQuery
                .GroupBy(item => new { item.UriId, item.Uri })
                .OrderBy(item => item.Key.Uri)
                .Select(item => new
                {
                    Uri = item.Key,
                    Count = item.Count()
                });

            var itemsTotal = await query.CountAsync();

            var pages = await query
                .Skip(filters.From)
                .Take(filters.Count)
                .ToListAsync();

            result.TotalPages = itemsTotal;
            result.Pages = pages
                .Select(item => new Page()
                {
                    Path = item.Uri.Uri,
                    Segments = item.Count
                });

            return result;
        }
    }
}
