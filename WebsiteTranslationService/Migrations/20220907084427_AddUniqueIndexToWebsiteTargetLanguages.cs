﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
    public partial class AddUniqueIndexToWebsiteTargetLanguages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_website-target-languages_WebsiteId_LanguageId",
                table: "website-target-languages",
                columns: new[] { "WebsiteId", "LanguageId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_website-target-languages_WebsiteId_LanguageId",
                table: "website-target-languages");
        }
    }
}
