﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
    public partial class AddSegmentMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_segments_TextNormalized_WebsiteId_UriId_IsSEO",
                table: "segments");

            migrationBuilder.DropColumn(
                name: "IsSEO",
                table: "segments");

            migrationBuilder.AddColumn<long>(
                name: "SegmentMetadataId",
                table: "segments",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "segment-metadata",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsSEO = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    Tag = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Attribute = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    ReferencedAttribute = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_segment-metadata", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_segments_SegmentMetadataId",
                table: "segments",
                column: "SegmentMetadataId");

            migrationBuilder.CreateIndex(
                name: "IX_segments_TextNormalized_WebsiteId_UriId_SegmentMetadataId",
                table: "segments",
                columns: new[] { "TextNormalized", "WebsiteId", "UriId", "SegmentMetadataId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_segment-metadata_Attribute_ReferencedAttribute_Tag_IsSEO",
                table: "segment-metadata",
                columns: new[] { "Attribute", "ReferencedAttribute", "Tag", "IsSEO" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_segments_segment-metadata_SegmentMetadataId",
                table: "segments",
                column: "SegmentMetadataId",
                principalTable: "segment-metadata",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_segments_segment-metadata_SegmentMetadataId",
                table: "segments");

            migrationBuilder.DropTable(
                name: "segment-metadata");

            migrationBuilder.DropIndex(
                name: "IX_segments_SegmentMetadataId",
                table: "segments");

            migrationBuilder.DropIndex(
                name: "IX_segments_TextNormalized_WebsiteId_UriId_SegmentMetadataId",
                table: "segments");

            migrationBuilder.DropColumn(
                name: "SegmentMetadataId",
                table: "segments");

            migrationBuilder.AddColumn<bool>(
                name: "IsSEO",
                table: "segments",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_segments_TextNormalized_WebsiteId_UriId_IsSEO",
                table: "segments",
                columns: new[] { "TextNormalized", "WebsiteId", "UriId", "IsSEO" },
                unique: true);
        }
    }
}
