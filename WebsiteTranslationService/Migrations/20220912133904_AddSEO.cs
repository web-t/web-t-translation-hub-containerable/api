﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
    public partial class AddSEO : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_segments_TextNormalized_WebsiteId_Uri",
                table: "segments");

            migrationBuilder.AddColumn<bool>(
                name: "IsSEO",
                table: "segments",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UriId",
                table: "segments",
                type: "varchar(64)",
                maxLength: 64,
                nullable: false,
                defaultValue: "")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_segments_TextNormalized_WebsiteId_UriId_IsSEO",
                table: "segments",
                columns: new[] { "TextNormalized", "WebsiteId", "UriId", "IsSEO" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_segments_TextNormalized_WebsiteId_UriId_IsSEO",
                table: "segments");

            migrationBuilder.DropColumn(
                name: "IsSEO",
                table: "segments");

            migrationBuilder.DropColumn(
                name: "UriId",
                table: "segments");

            migrationBuilder.CreateIndex(
                name: "IX_segments_TextNormalized_WebsiteId_Uri",
                table: "segments",
                columns: new[] { "TextNormalized", "WebsiteId", "Uri" },
                unique: true);
        }
    }
}
