﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
    public partial class RemoveUnnecessaryWebsiteModelsAndReturnForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_segment-translations_segments_SegmentId1",
                table: "segment-translations");

            migrationBuilder.DropForeignKey(
                name: "FK_segment-translations_website-target-languages_WebsiteTargetL~",
                table: "segment-translations");

            migrationBuilder.DropForeignKey(
                name: "FK_segments_websites_WebsiteId",
                table: "segments");

            migrationBuilder.DropTable(
                name: "domains");

            migrationBuilder.DropTable(
                name: "languages");

            migrationBuilder.DropTable(
                name: "website-target-languages");

            migrationBuilder.DropTable(
                name: "websites");

            migrationBuilder.DropIndex(
                name: "IX_segments_WebsiteId",
                table: "segments");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_segment-translations_SegmentId",
                table: "segment-translations");

            migrationBuilder.DropIndex(
                name: "IX_segment-translations_SegmentId1",
                table: "segment-translations");

            migrationBuilder.DropIndex(
                name: "IX_segment-translations_WebsiteTargetLanguageId",
                table: "segment-translations");

            migrationBuilder.DropColumn(
                name: "SegmentId1",
                table: "segment-translations");

            migrationBuilder.DropColumn(
                name: "WebsiteTargetLanguageId",
                table: "segment-translations");

            migrationBuilder.AlterColumn<string>(
                name: "WebsiteTargetLanguageName",
                table: "segment-translations",
                type: "varchar(255)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_segment-translations_SegmentId_WebsiteTargetLanguageName",
                table: "segment-translations",
                columns: new[] { "SegmentId", "WebsiteTargetLanguageName" });

            migrationBuilder.AddForeignKey(
                name: "FK_segment-translations_segments_SegmentId",
                table: "segment-translations",
                column: "SegmentId",
                principalTable: "segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_segment-translations_segments_SegmentId",
                table: "segment-translations");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_segment-translations_SegmentId_WebsiteTargetLanguageName",
                table: "segment-translations");

            migrationBuilder.AlterColumn<string>(
                name: "WebsiteTargetLanguageName",
                table: "segment-translations",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(255)")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<long>(
                name: "SegmentId1",
                table: "segment-translations",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "WebsiteTargetLanguageId",
                table: "segment-translations",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_segment-translations_SegmentId",
                table: "segment-translations",
                column: "SegmentId");

            migrationBuilder.CreateTable(
                name: "domains",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_domains", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "languages",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_languages", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "website-target-languages",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_website-target-languages", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "websites",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Name = table.Column<string>(type: "varchar(150)", maxLength: 150, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    User = table.Column<string>(type: "varchar(36)", maxLength: 36, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_websites", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_segments_WebsiteId",
                table: "segments",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_segment-translations_SegmentId1",
                table: "segment-translations",
                column: "SegmentId1");

            migrationBuilder.CreateIndex(
                name: "IX_segment-translations_WebsiteTargetLanguageId",
                table: "segment-translations",
                column: "WebsiteTargetLanguageId");

            migrationBuilder.AddForeignKey(
                name: "FK_segment-translations_segments_SegmentId1",
                table: "segment-translations",
                column: "SegmentId1",
                principalTable: "segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_segment-translations_website-target-languages_WebsiteTargetL~",
                table: "segment-translations",
                column: "WebsiteTargetLanguageId",
                principalTable: "website-target-languages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_segments_websites_WebsiteId",
                table: "segments",
                column: "WebsiteId",
                principalTable: "websites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
