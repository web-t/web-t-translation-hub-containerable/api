﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
    public partial class AddConfigurableWebsiteName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "websites",
                type: "varchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "")
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "websites");
        }
    }
}
