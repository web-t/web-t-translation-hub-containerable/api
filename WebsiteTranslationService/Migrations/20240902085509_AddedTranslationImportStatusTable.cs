﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
	/// <inheritdoc />
	public partial class AddedTranslationImportStatusTable : Migration
	{
		/// <inheritdoc />
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				name: "TranslationImportStatuses",
				columns: table => new
				{
					ConfigurationId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
					ImportStatus = table.Column<int>(type: "tinyint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_TranslationImportStatuses", x => x.ConfigurationId);
				})
				.Annotation("MySql:CharSet", "utf8mb4");
		}

		/// <inheritdoc />
		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				name: "TranslationImportStatuses");
		}
	}
}
