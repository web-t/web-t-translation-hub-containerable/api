﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
    public partial class DropAllForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_domains_websites_WebsiteId",
                table: "domains");

            migrationBuilder.DropForeignKey(
                name: "FK_segment-translations_segments_SegmentId",
                table: "segment-translations");

            migrationBuilder.DropForeignKey(
                name: "FK_website-target-languages_languages_LanguageId",
                table: "website-target-languages");

            migrationBuilder.DropForeignKey(
                name: "FK_website-target-languages_websites_WebsiteId",
                table: "website-target-languages");

            migrationBuilder.DropForeignKey(
                name: "FK_websites_languages_SourceLanguageId",
                table: "websites");

            migrationBuilder.DropIndex(
                name: "IX_websites_SourceLanguageId",
                table: "websites");

            migrationBuilder.DropIndex(
                name: "IX_website-target-languages_LanguageId",
                table: "website-target-languages");

            migrationBuilder.DropIndex(
                name: "IX_website-target-languages_WebsiteId_LanguageId",
                table: "website-target-languages");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_segment-translations_SegmentId_WebsiteTargetLanguageId",
                table: "segment-translations");

            migrationBuilder.DropIndex(
                name: "IX_languages_Name",
                table: "languages");

            migrationBuilder.DropIndex(
                name: "IX_domains_WebsiteId",
                table: "domains");

            migrationBuilder.DropColumn(
                name: "SourceLanguageId",
                table: "websites");

            migrationBuilder.DropColumn(
                name: "LanguageId",
                table: "website-target-languages");

            migrationBuilder.DropColumn(
                name: "WebsiteId",
                table: "website-target-languages");

            migrationBuilder.DropColumn(
                name: "WebsiteId",
                table: "domains");

            migrationBuilder.AddColumn<long>(
                name: "SegmentId1",
                table: "segment-translations",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<Guid>(
                name: "WebsiteId",
                table: "segment-translations",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddColumn<string>(
                name: "WebsiteTargetLanguageName",
                table: "segment-translations",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "languages",
                type: "longtext",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(255)")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_segment-translations_SegmentId",
                table: "segment-translations",
                column: "SegmentId");

            migrationBuilder.CreateIndex(
                name: "IX_segment-translations_SegmentId1",
                table: "segment-translations",
                column: "SegmentId1");

            migrationBuilder.AddForeignKey(
                name: "FK_segment-translations_segments_SegmentId1",
                table: "segment-translations",
                column: "SegmentId1",
                principalTable: "segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_segment-translations_segments_SegmentId1",
                table: "segment-translations");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_segment-translations_SegmentId",
                table: "segment-translations");

            migrationBuilder.DropIndex(
                name: "IX_segment-translations_SegmentId1",
                table: "segment-translations");

            migrationBuilder.DropColumn(
                name: "SegmentId1",
                table: "segment-translations");

            migrationBuilder.DropColumn(
                name: "WebsiteId",
                table: "segment-translations");

            migrationBuilder.DropColumn(
                name: "WebsiteTargetLanguageName",
                table: "segment-translations");

            migrationBuilder.AddColumn<long>(
                name: "SourceLanguageId",
                table: "websites",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LanguageId",
                table: "website-target-languages",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<Guid>(
                name: "WebsiteId",
                table: "website-target-languages",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "languages",
                type: "varchar(255)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "longtext")
                .Annotation("MySql:CharSet", "utf8mb4")
                .OldAnnotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<Guid>(
                name: "WebsiteId",
                table: "domains",
                type: "char(36)",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                collation: "ascii_general_ci");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_segment-translations_SegmentId_WebsiteTargetLanguageId",
                table: "segment-translations",
                columns: new[] { "SegmentId", "WebsiteTargetLanguageId" });

            migrationBuilder.CreateIndex(
                name: "IX_websites_SourceLanguageId",
                table: "websites",
                column: "SourceLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_website-target-languages_LanguageId",
                table: "website-target-languages",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_website-target-languages_WebsiteId_LanguageId",
                table: "website-target-languages",
                columns: new[] { "WebsiteId", "LanguageId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_languages_Name",
                table: "languages",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_domains_WebsiteId",
                table: "domains",
                column: "WebsiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_domains_websites_WebsiteId",
                table: "domains",
                column: "WebsiteId",
                principalTable: "websites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_segment-translations_segments_SegmentId",
                table: "segment-translations",
                column: "SegmentId",
                principalTable: "segments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_website-target-languages_languages_LanguageId",
                table: "website-target-languages",
                column: "LanguageId",
                principalTable: "languages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_website-target-languages_websites_WebsiteId",
                table: "website-target-languages",
                column: "WebsiteId",
                principalTable: "websites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_websites_languages_SourceLanguageId",
                table: "websites",
                column: "SourceLanguageId",
                principalTable: "languages",
                principalColumn: "Id");
        }
    }
}
