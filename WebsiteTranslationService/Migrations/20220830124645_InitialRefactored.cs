﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tilde.MT.WebsiteTranslationService.Migrations
{
    public partial class InitialRefactored : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "languages",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(255)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_languages", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "translations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Text = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Origin = table.Column<int>(type: "int", nullable: false),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_translations", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "websites",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    User = table.Column<string>(type: "varchar(36)", maxLength: 36, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    SourceLanguageId = table.Column<long>(type: "bigint", nullable: true),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_websites", x => x.Id);
                    table.ForeignKey(
                        name: "FK_websites_languages_SourceLanguageId",
                        column: x => x.SourceLanguageId,
                        principalTable: "languages",
                        principalColumn: "Id");
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "domains",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "longtext", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    WebsiteId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_domains", x => x.Id);
                    table.ForeignKey(
                        name: "FK_domains_websites_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "websites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "segments",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TextNormalized = table.Column<string>(type: "varchar(64)", maxLength: 64, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    TextRaw = table.Column<string>(type: "varchar(5000)", maxLength: 5000, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    Uri = table.Column<string>(type: "varchar(512)", maxLength: 512, nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    WebsiteId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_segments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_segments_websites_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "websites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "website-target-languages",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    WebsiteId = table.Column<Guid>(type: "char(36)", nullable: false, collation: "ascii_general_ci"),
                    LanguageId = table.Column<long>(type: "bigint", nullable: false),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_website-target-languages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_website-target-languages_languages_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_website-target-languages_websites_WebsiteId",
                        column: x => x.WebsiteId,
                        principalTable: "websites",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "segment-translations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Status = table.Column<int>(type: "int", nullable: false),
                    WebsiteTargetLanguageId = table.Column<long>(type: "bigint", nullable: false),
                    AcceptedTranslationId = table.Column<long>(type: "bigint", nullable: true),
                    SegmentId = table.Column<long>(type: "bigint", nullable: false),
                    DbCreatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    DbUpdatedAt = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_segment-translations", x => x.Id);
                    table.UniqueConstraint("AK_segment-translations_SegmentId_WebsiteTargetLanguageId", x => new { x.SegmentId, x.WebsiteTargetLanguageId });
                    table.ForeignKey(
                        name: "FK_segment-translations_segments_SegmentId",
                        column: x => x.SegmentId,
                        principalTable: "segments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_segment-translations_translations_AcceptedTranslationId",
                        column: x => x.AcceptedTranslationId,
                        principalTable: "translations",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_segment-translations_website-target-languages_WebsiteTargetL~",
                        column: x => x.WebsiteTargetLanguageId,
                        principalTable: "website-target-languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateTable(
                name: "SegmentTranslationHistories",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TranslationId = table.Column<long>(type: "bigint", nullable: false),
                    SegmentTranslationId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SegmentTranslationHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SegmentTranslationHistories_segment-translations_SegmentTran~",
                        column: x => x.SegmentTranslationId,
                        principalTable: "segment-translations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SegmentTranslationHistories_translations_TranslationId",
                        column: x => x.TranslationId,
                        principalTable: "translations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_domains_WebsiteId",
                table: "domains",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_languages_Name",
                table: "languages",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_segment-translations_AcceptedTranslationId",
                table: "segment-translations",
                column: "AcceptedTranslationId");

            migrationBuilder.CreateIndex(
                name: "IX_segment-translations_WebsiteTargetLanguageId",
                table: "segment-translations",
                column: "WebsiteTargetLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_segments_TextNormalized",
                table: "segments",
                column: "TextNormalized")
                .Annotation("MySql:IndexPrefixLength", new[] { 64 });

            migrationBuilder.CreateIndex(
                name: "IX_segments_TextNormalized_WebsiteId_Uri",
                table: "segments",
                columns: new[] { "TextNormalized", "WebsiteId", "Uri" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_segments_TextRaw",
                table: "segments",
                column: "TextRaw")
                .Annotation("MySql:IndexPrefixLength", new[] { 768 });

            migrationBuilder.CreateIndex(
                name: "IX_segments_WebsiteId",
                table: "segments",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SegmentTranslationHistories_SegmentTranslationId",
                table: "SegmentTranslationHistories",
                column: "SegmentTranslationId");

            migrationBuilder.CreateIndex(
                name: "IX_SegmentTranslationHistories_TranslationId_SegmentTranslation~",
                table: "SegmentTranslationHistories",
                columns: new[] { "TranslationId", "SegmentTranslationId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_website-target-languages_LanguageId",
                table: "website-target-languages",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_website-target-languages_WebsiteId",
                table: "website-target-languages",
                column: "WebsiteId");

            migrationBuilder.CreateIndex(
                name: "IX_websites_SourceLanguageId",
                table: "websites",
                column: "SourceLanguageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "domains");

            migrationBuilder.DropTable(
                name: "SegmentTranslationHistories");

            migrationBuilder.DropTable(
                name: "segment-translations");

            migrationBuilder.DropTable(
                name: "segments");

            migrationBuilder.DropTable(
                name: "translations");

            migrationBuilder.DropTable(
                name: "website-target-languages");

            migrationBuilder.DropTable(
                name: "websites");

            migrationBuilder.DropTable(
                name: "languages");
        }
    }
}
