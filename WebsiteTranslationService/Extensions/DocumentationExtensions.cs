﻿using Microsoft.OpenApi.Models;
using Tilde.MT.WebsiteTranslationService.Models.Configuration;

namespace Tilde.MT.WebsiteTranslationService.Extensions
{
    public static class DocumentationExtensions
    {
        /// <summary>
        /// Add swagger documentation
        /// </summary>
        /// <param name="services"> Service collection </param>
        /// <param name="configuration"> Configuration settings </param>
        /// <returns></returns>
        public static IServiceCollection AddDocumentation(this IServiceCollection services, ConfigurationSettings configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = nameof(Tilde.MT.WebsiteTranslationService), Version = "v1" });

                c.EnableAnnotations();

                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{nameof(Tilde)}.{nameof(Tilde.MT)}.{nameof(Tilde.MT.WebsiteTranslationService)}.xml"));

                c.AddServer(new OpenApiServer()
                {
                    Url = configuration.BaseUrl
                });
            });

            return services;
        }

        /// <summary>
        /// Use documentation
        /// </summary>
        /// <param name="app"> Application builder </param>
        /// <param name="configuration"> Configuration settings </param>
        /// <returns></returns>

        public static IApplicationBuilder UseDocumentation(this IApplicationBuilder app, ConfigurationSettings configuration)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint(configuration.BaseUrl + "/swagger/v1/swagger.json", $"{nameof(Tilde.MT.WebsiteTranslationService)} v1"));

            return app;
        }
    }
}
