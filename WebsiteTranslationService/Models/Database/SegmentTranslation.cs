﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tilde.MT.WebsiteTranslationService.Enums.Segment;
using Tilde.MT.WebsiteTranslationService.Interfaces.Database;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
    [Table("segment-translations")]
    public class SegmentTranslation : IEntityTimestamps
    {
        /// <summary>
        /// Segment id 
        /// </summary>
        [Key]
        [DatabaseGenerat‌ed(DatabaseGeneratedOp‌​tion.Identity)]
        public long Id { get; set; }

        public SegmentTranslationStatus Status { get; set; }

        #region Relations
        public Guid WebsiteId { get; set; }
        public string WebsiteTargetLanguageName { get; set; }

        public Translation? AcceptedTranslation { get; set; } = null;

        [ForeignKey(nameof(Segment))]
        public long SegmentId { get; set; }
        public Segment Segment { get; set; }

        public ICollection<SegmentTranslationHistory> SegmentTranslationHistory { get; set; }

        #endregion

        #region Timestamps

        public DateTime? DbCreatedAt { get; set; }
        public DateTime? DbUpdatedAt { get; set; }

        #endregion
    }
}
