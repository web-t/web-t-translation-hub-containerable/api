﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
	public class ServiceDbContext : DbContext
	{
		/// <summary>
		/// Segments for website
		/// </summary>
		public DbSet<Segment> Segments { get; set; }

		/// <summary>
		/// Segment translations
		/// </summary>
		public DbSet<SegmentTranslation> SegmentTranslations { get; set; }

		/// <summary>
		/// Translations for segments
		/// </summary>
		public DbSet<Translation> Translations { get; set; }

		/// <summary>
		/// Segment translation histories to circumvent circular dependency for segment-translations and translations
		/// </summary>
		public DbSet<SegmentTranslationHistory> SegmentTranslationHistories { get; set; }

		/// <summary>
		/// Segment metadata
		/// </summary>
		public DbSet<SegmentMetadata> SegmentMetadata { get; set; }

		/// <summary>
		/// Translation import statuses of configurations
		/// </summary>
		public DbSet<TranslationImportStatus> TranslationImportStatuses { get; set; }

		public ServiceDbContext(DbContextOptions<ServiceDbContext> options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			// Entity framework fetches by default unspecified format, but it should be UTC
			// https://stackoverflow.com/questions/4648540/entity-framework-datetime-and-utc
			var dateTimeConverter = new ValueConverter<DateTime, DateTime>(
				item => item.ToUniversalTime(),
				item => DateTime.SpecifyKind(item, DateTimeKind.Utc));

			var nullableDateTimeConverter = new ValueConverter<DateTime?, DateTime?>(
				item => item.HasValue ? item.Value.ToUniversalTime() : item,
				item => item.HasValue ? DateTime.SpecifyKind(item.Value, DateTimeKind.Utc) : item);

			foreach (var entityType in builder.Model.GetEntityTypes())
			{
				if (entityType.IsKeyless)
				{
					continue;
				}

				foreach (var property in entityType.GetProperties())
				{
					if (property.ClrType == typeof(DateTime))
					{
						property.SetValueConverter(dateTimeConverter);
					}
					else if (property.ClrType == typeof(DateTime?))
					{
						property.SetValueConverter(nullableDateTimeConverter);
					}
				}
			}

			builder.Entity<Segment>()
				.HasIndex(item => new
				{
					item.TextNormalized
				})
				// MySQL InnoDB index limit 3072 bytes
				.HasPrefixLength(64);

			builder.Entity<Segment>()
				.HasIndex(item => new
				{
					item.TextRaw
				})
				// MySQL InnoDB index limit 3072 bytes
				.HasPrefixLength(768);

			builder.Entity<Segment>()
				.HasIndex(item => new
				{
					item.TextNormalized,
					item.WebsiteId,
					item.UriId,
					item.SegmentMetadataId
				})
				.IsUnique();

			builder.Entity<SegmentMetadata>()
				.HasIndex(item => new
				{
					item.Attribute,
					item.ReferencedAttribute,
					item.Tag,
					item.IsSEO
				})
				.IsUnique();

			builder.Entity<SegmentTranslation>().HasAlternateKey(item => new
			{
				item.SegmentId,
				item.WebsiteTargetLanguageName
			});

			builder.Entity<SegmentTranslationHistory>()
				.HasIndex(item => new
				{
					item.TranslationId,
					item.SegmentTranslationId
				})
				.IsUnique();
		}
	}
}
