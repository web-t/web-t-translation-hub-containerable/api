﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
    [Table("segment-metadata")]
    public record SegmentMetadata
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public bool IsSEO { get; init; }

        [MaxLength(100)]
        public string Tag { get; init; }

        [MaxLength(100)]
        public string? Attribute { get; init; }

        [MaxLength(100)]
        public string? ReferencedAttribute { get; init; }

        #region Relations

        public ICollection<Segment> SegmentTranslations { get; set; }

        #endregion
    }
}
