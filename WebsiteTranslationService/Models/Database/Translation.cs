﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tilde.MT.WebsiteTranslationService.Enums.Translation;
using Tilde.MT.WebsiteTranslationService.Interfaces.Database;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
    [Table("translations")]
    public class Translation : IEntityTimestamps
    {
        [Key]
        [DatabaseGenerat‌ed(DatabaseGeneratedOp‌​tion.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// TextNormalized text
        /// </summary>
        public string Text { get; set; }

        public TranslationOrigin Origin { get; set; }

        #region Relations 

        public ICollection<SegmentTranslationHistory> SegmentTranslationHistory { get; set; }

        #endregion

        #region Timestamps

        public DateTime? DbCreatedAt { get; set; }
        public DateTime? DbUpdatedAt { get; set; }

        #endregion
    }
}
