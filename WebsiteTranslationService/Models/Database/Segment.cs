﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tilde.MT.WebsiteTranslationService.Interfaces.Database;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
    [Table("segments")]
    public class Segment : IEntityTimestamps
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        /// <summary>
        /// Source text (Normalized). Used for searching segments. 
        /// 
        /// MySQL InnoDB index limit 3072 bytes, so hash it for uniqueness
        /// </summary>
        [MaxLength(64)]
        public string TextNormalized { get; set; }

        /// <summary>
        /// Raw source text, can contain additional user data for segment preview etc. (html css attributes, tag names ...)
        /// </summary>
        [MaxLength(5000)]
        public string TextRaw { get; set; }

        [MaxLength(512)]
        public string Uri { get; set; }

        /// <summary>
        /// Uri identifier uri can be 1024 or more symbols 
        /// 
        /// MySQL InnoDB index limit 3072 bytes, so hash it for uniqueness
        /// </summary>
        [MaxLength(64)]
        public string UriId { get; set; }

        #region Relations

        public Guid WebsiteId { get; set; }

        [ForeignKey(nameof(SegmentMetadata))]
        public long SegmentMetadataId { get; set; }
        public SegmentMetadata SegmentMetadata { get; set; }


        public ICollection<SegmentTranslation> SegmentTranslations { get; set; }

        #endregion

        #region Timestamps

        public DateTime? DbCreatedAt { get; set; }
        public DateTime? DbUpdatedAt { get; set; }

        #endregion
    }
}
