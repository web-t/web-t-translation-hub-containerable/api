﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
    public class SegmentTranslationHistory
    {
        [Key]
        [DatabaseGenerat‌ed(DatabaseGeneratedOp‌​tion.Identity)]
        public long Id { get; set; }

        #region Relations

        [ForeignKey(nameof(TranslationId))]
        public long TranslationId { get; set; }
        public Translation Translation { get; set; }

        [ForeignKey(nameof(SegmentTranslation))]
        public long SegmentTranslationId { get; set; }
        public SegmentTranslation SegmentTranslation { get; set; }

        #endregion
    }
}
