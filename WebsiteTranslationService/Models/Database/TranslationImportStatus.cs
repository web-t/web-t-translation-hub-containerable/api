﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Tilde.MT.WebsiteTranslationService.Enums.Import;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
	public class TranslationImportStatus
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public Guid ConfigurationId { get; set; }

		public ImportStatus ImportStatus { get; set; }
	}
}
