﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tilde.MT.WebsiteTranslationService.Models.Database
{
    public class SegmentUri
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(512)]
        public string Uri { get; set; }

        /// <summary>
        /// Uri identifier uri can be 1024 or more symbols 
        /// 
        /// MySQL InnoDB index limit 3072 bytes, so hash it for uniqueness
        /// </summary>
        [MaxLength(64)]
        public string UriId { get; set; }

        public Guid WebsiteId { get; set; }

    }
}
