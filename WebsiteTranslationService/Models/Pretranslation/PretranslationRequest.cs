﻿using System.ComponentModel.DataAnnotations;

namespace Tilde.MT.WebsiteTranslationService.Models.Pretranslation
{
	public class PretranslationRequest
	{
		[Required]
		public Guid ConfigurationId { get; set; }

		[Required]
		public string Domain { get; set; }

		[Required]
		public List<string> TargetLanguages { get; set; }
	}
}
