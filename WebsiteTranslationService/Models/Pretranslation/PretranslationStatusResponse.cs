﻿using System.Text.Json.Serialization;
using Tilde.MT.WebsiteTranslationService.Enums.Pretranslation;

namespace Tilde.MT.WebsiteTranslationService.Models.Pretranslation
{
	public class PretranslationStatusResponse
	{
		[JsonPropertyName("pretranslatedUrls")]
		public int PretranslatedUrls { get; set; }

		[JsonPropertyName("totalUrls")]
		public int TotalUrls { get; set; }

		[JsonPropertyName("status")]
		[JsonConverter(typeof(JsonStringEnumConverter))]
		public PretranslationStatus PretranslationStatus { get; set; }
	}
}
