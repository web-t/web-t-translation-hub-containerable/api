﻿using System.ComponentModel.DataAnnotations;

namespace Tilde.MT.WebsiteTranslationService.Models.TranslationImport
{
	public class ImportFields
	{
		[Required]
		public IFormFile xliffFile { get; set; }

		[Required]
		public Guid configId { get; set; }
	}
}
