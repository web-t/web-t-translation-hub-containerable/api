﻿namespace Tilde.MT.WebsiteTranslationService.Models.TranslationImport
{
	public class TranslationForImport
	{
		public string SourceLanguage { get; set; }
		public string TargetLanguage { get; set; }

		public string SourceText { get; set; }

		public string TargetText { get; set; }

		public DateTime? CreatedTime { get; set; }

		public DateTime? UpdatedTime { get; set; }

		public bool IsHumanTranslation { get; set; }

		public string IsAccepted { get; set; }

		public string UriPath { get; set; } = "/";

		public string MetadataTag { get; set; }

		public bool? IsSeo { get; set; }

		public string? Attribute { get; set; }
		public string? ReferencedAttribute { get; set; }
	}
}
