﻿using System.Text.Json.Serialization;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Translation;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO
{
    public class SegmentList
    {
        [JsonPropertyName("totalSegments")]
        public int TotalSegments { get; set; }

        [JsonPropertyName("segments")]
        public IEnumerable<Segment> Segments { get; set; }
    }
}
