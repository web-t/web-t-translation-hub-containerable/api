﻿using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Page
{
    public class Page
    {
        [JsonPropertyName("path")]
        public string Path { get; set; }

        [JsonPropertyName("segments")]
        public long Segments { get; set; }
    }
}
