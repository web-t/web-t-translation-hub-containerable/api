﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Page
{
    public class PageListFilter
    {
        [FromQuery(Name = "text")]
        public string? Text { get; set; }

        #region Pagination

        [Required]
        [FromQuery(Name = "from")]
        [Range(0, int.MaxValue)]
        public int From { get; set; }

        [Required]
        [FromQuery(Name = "count")]
        [Range(1, 100)]
        public int Count { get; set; }

        #endregion
    }
}
