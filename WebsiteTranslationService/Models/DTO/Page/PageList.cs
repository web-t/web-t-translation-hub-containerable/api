﻿using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Page
{
    public class PageList
    {
        [JsonPropertyName("totalPages")]
        public long TotalPages { get; set; }

        [JsonPropertyName("pages")]
        public IEnumerable<Page> Pages { get; set; }
    }
}
