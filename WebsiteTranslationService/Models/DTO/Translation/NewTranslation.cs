﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    public class NewTranslation
    {
        [JsonPropertyName("text")]
        [MaxLength(10_000)]
        public string Text { get; set; }

        [JsonPropertyName("URL")]
        [MaxLength(512)]
        public string Uri { get; set; }
    }
}
