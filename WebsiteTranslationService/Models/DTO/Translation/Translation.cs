﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Tilde.MT.WebsiteTranslationService.Enums.Segment;
using Tilde.MT.WebsiteTranslationService.Enums.Translation;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    public class Translation
    {
        /// <summary>
        /// TextNormalized id
        /// </summary>
        [JsonPropertyName("id")]
        public long Id { get; set; }

        /// <summary>
        /// Suggested text
        /// </summary>
        [MaxLength(5000)]
        [JsonPropertyName("text")]
        public string Text { get; set; }

        /// <summary>
        /// Translation language
        /// </summary>
        [JsonPropertyName("language")]
        public string Language { get; set; }

        /// <summary>
        /// Translation status
        /// </summary>
        [JsonPropertyName("status")]
        public SegmentTranslationStatus Status { get; set; }

        /// <summary>
        /// Translation origin
        /// </summary>
        [JsonPropertyName("origin")]
        public TranslationOrigin Origin { get; set; }
    }
}
