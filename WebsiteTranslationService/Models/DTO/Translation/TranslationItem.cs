﻿using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    /// <summary>
    /// Translated information for one translation
    /// </summary>
    public class TranslationItem
    {
        /// <summary>
        /// Source segment id
        /// </summary>
        [JsonPropertyName("segmentId")]
        public long SourceSegmentId { get; set; }

        /// <summary>
        /// Translated text
        /// </summary>
        [JsonPropertyName("translation")]
        public string Translation { get; init; }
    }
}
