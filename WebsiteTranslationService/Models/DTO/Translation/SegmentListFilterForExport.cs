﻿using Microsoft.AspNetCore.Mvc;
using Tilde.MT.WebsiteTranslationService.Enums.Segment;
using Tilde.MT.WebsiteTranslationService.Enums.Translation;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
	public class SegmentListFilterForExport
	{
		[FromQuery(Name = "origin")]
		public TranslationOrigin? Origin { get; set; }

		[FromQuery(Name = "status")]
		public SegmentTranslationStatus? Status { get; set; }

		[FromQuery(Name = "seo")]
		public bool? IsSEO { get; set; }

		[FromQuery(Name = "targetLanguages")]
		public string[]? TargetLanguages { get; set; }
	}
}
