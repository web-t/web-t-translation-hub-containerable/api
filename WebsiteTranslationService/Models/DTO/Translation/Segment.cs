﻿using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    public class Segment
    {
        /// <summary>
        /// Segment id
        /// </summary>
        [JsonPropertyName("id")]
        public long Id { get; set; }

        /// <summary>
        /// Source text
        /// </summary>
        [JsonPropertyName("text")]
        public string Text { get; set; }

        /// <summary>
        /// Segment uri
        /// </summary>
        [JsonPropertyName("uri")]
        public string Uri { get; set; }

        /// <summary>
        /// Translated text
        /// </summary>
        [JsonPropertyName("translations")]
        public IEnumerable<Translation> Translations { get; set; }

        /// <summary>
        /// Metadata
        /// </summary>
        [JsonPropertyName("meta")]
        public TextMetadata Metadata { get; set; } = new TextMetadata();
    }
}
