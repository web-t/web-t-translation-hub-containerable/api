﻿using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    public class TextMetadata
    {
        [JsonPropertyName("seo")]
        public bool IsSEO { get; init; }

        [JsonPropertyName("tag")]
        public string Tag { get; init; }

        [JsonPropertyName("attr")]
        public string? Attribute { get; init; }

        [JsonPropertyName("refAttr")]
        public string? ReferencedAttribute { get; init; }
    }
}
