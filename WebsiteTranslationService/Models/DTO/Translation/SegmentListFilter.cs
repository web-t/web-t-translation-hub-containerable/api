﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Tilde.MT.WebsiteTranslationService.Enums.Segment;
using Tilde.MT.WebsiteTranslationService.Enums.Translation;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    public class SegmentListFilter
    {
        [MinLength(2)]
        [MaxLength(2)]
        [FromQuery(Name = "lang")]
        public string? Language { get; set; }

        [FromQuery(Name = "origin")]
        public TranslationOrigin? Origin { get; set; }

        [FromQuery(Name = "status")]
        public SegmentTranslationStatus? Status { get; set; }

        [FromQuery(Name = "text")]
        public string? Text { get; set; }

        [MaxLength(512)]
        [FromQuery(Name = "url")]
        public string? Uri { get; set; }

        [FromQuery(Name = "seo")]
        public bool? IsSEO { get; set; }

        #region Pagination

        [Required]
        [FromQuery(Name = "from")]
        [Range(0, int.MaxValue)]
        public int From { get; set; }

        [Required]
        [FromQuery(Name = "count")]
        [Range(1, 100)]
        public int Count { get; set; }

        #endregion
    }
}
