﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    public class SegmentRequest
    {
        [Required]
        [MaxLength(512)]
        [JsonPropertyName("URL")]
        public string Uri { get; set; }

        /// <summary>
        /// The language to translate text to. Two-byte language code according to ISO 639-1.
        /// </summary>
        /// <example>ru</example>
        [Required]
        [MaxLength(2)]
        [JsonPropertyName("lang")]
        public string TargetLanguage { get; init; }

        /// <summary>
        /// Array of text segments to translate
        /// </summary>
        [JsonPropertyName("texts")]
        [MaxLength(30)]
        public List<SegmentRequestText> Text { get; init; }
    }
}
