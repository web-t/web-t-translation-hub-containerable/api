﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Tilde.MT.WebsiteTranslationService.Models.DTO.Translation
{
    public class SegmentRequestText
    {
        /// <summary>
        /// Text to translate
        /// </summary>
        /// <example>"Segment to translate"</example>
        [JsonPropertyName("text")]
        [MaxLength(10_000)]
        [MinLength(0)]
        public string Text { get; init; }

        /// <summary>
        /// Text to translate
        /// </summary>
        [JsonPropertyName("meta")]
        public TextMetadata Metadata { get; init; }
    }
}
