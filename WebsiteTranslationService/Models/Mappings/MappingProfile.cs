﻿using AutoMapper;

namespace Tilde.MT.WebsiteTranslationService.Models.Mappings
{
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Custom mapping functions for mapping data between database and data transfer object models
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Models.Database.SegmentMetadata, Models.DTO.Translation.TextMetadata>()
                .ForMember(dest => dest.IsSEO, opt => opt.MapFrom(src => src.IsSEO))
                .ForMember(dest => dest.Attribute, opt => opt.MapFrom(src => src.Attribute))
                .ForMember(dest => dest.ReferencedAttribute, opt => opt.MapFrom(src => src.ReferencedAttribute))
                .ForMember(dest => dest.Tag, opt => opt.MapFrom(src => src.Tag));

            CreateMap<Models.Database.Segment, Models.DTO.Translation.Segment>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.TextRaw))
                .ForMember(dest => dest.Translations, opt => opt.MapFrom(src => src.SegmentTranslations))
                .ForMember(dest => dest.Uri, opt => opt.MapFrom(src => src.Uri))
                .ForMember(dest => dest.Metadata, opt => opt.MapFrom(src => src.SegmentMetadata));

            CreateMap<Models.Database.SegmentTranslation, Models.DTO.Translation.Translation>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AcceptedTranslation.Id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.AcceptedTranslation.Text))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Origin, opt => opt.MapFrom(src => src.AcceptedTranslation.Origin))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.WebsiteTargetLanguageName));

            CreateMap<Models.Database.Translation, Models.DTO.Translation.Translation>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Text))
                .ForMember(dest => dest.Language, opt => opt.MapFrom(src => src.SegmentTranslationHistory.First().SegmentTranslation.WebsiteTargetLanguageName));
        }
    }
}
