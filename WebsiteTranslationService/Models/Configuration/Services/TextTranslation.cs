﻿namespace Tilde.MT.WebsiteTranslationService.Models.Configuration.Services
{
    public class TextTranslation
    {
        /// <summary>
        /// Text translation service URI 
        /// </summary>
        public string Uri { get; init; }
    }
}
