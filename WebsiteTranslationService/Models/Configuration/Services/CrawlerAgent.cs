﻿namespace Tilde.MT.WebsiteTranslationService.Models.Configuration.Services
{
	public class CrawlerAgent
	{
		/// <summary>
		/// Crawler agent service URI 
		/// </summary>
		public string Uri { get; init; }
	}
}
