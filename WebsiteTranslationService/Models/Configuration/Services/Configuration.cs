﻿namespace Tilde.MT.WebsiteTranslationService.Models.Configuration.Services
{
    public class Configuration
    {
        /// <summary>
        /// Configuration service URI
        /// </summary>
        public string Uri { get; init; }
    }
}
