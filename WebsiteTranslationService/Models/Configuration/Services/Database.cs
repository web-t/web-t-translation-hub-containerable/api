﻿namespace Tilde.MT.WebsiteTranslationService.Models.Configuration.Services
{
    public class Database
    {
        /// <summary>
        /// Database connection string 
        /// </summary>
        public string ConnectionString { get; init; }
    }
}
