﻿namespace Tilde.MT.WebsiteTranslationService.Models.Configuration
{
	public class ConfigurationServices
	{
		public Services.Database Database { get; init; }
		public Services.TextTranslation TextTranslation { get; init; }
		public Services.Configuration Configuration { get; init; }
		public Services.CrawlerAgent CrawlerAgent { get; init; }
	}
}
