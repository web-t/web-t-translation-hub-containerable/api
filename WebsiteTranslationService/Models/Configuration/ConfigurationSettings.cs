﻿namespace Tilde.MT.WebsiteTranslationService.Models.Configuration
{
	public class ConfigurationSettings
	{
		/// <summary>
		/// Base URL of the service
		/// </summary>
		public string BaseUrl { get; set; }

		/// <summary>
		/// Max allowed file size when importing website translations
		/// </summary>
		public int MaxImportFileSize { get; set; }
	}
}
