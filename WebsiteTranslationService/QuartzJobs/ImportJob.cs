﻿using Microsoft.EntityFrameworkCore;
using Quartz;
using System.Text.Json;
using Tilde.EMW.Contracts.Models.Services.Configuration.Configuration;
using Tilde.MT.WebsiteTranslationService.Exceptions.Import;
using Tilde.MT.WebsiteTranslationService.Interfaces.Providers;
using Tilde.MT.WebsiteTranslationService.Models.Database;
using Tilde.MT.WebsiteTranslationService.Models.TranslationImport;
using Tilde.MT.WebsiteTranslationService.Services;

namespace Tilde.MT.WebsiteTranslationService.QuartzJobs
{
	public class ImportJob : IJob
	{
		private readonly ILogger _logger;
		private readonly ImportService _importService;
		private readonly ISegmentNormalizerProvider _segmentNormalizerProvider;
		private readonly ServiceDbContext _dbContext;

		private readonly string DefaultTag = "UNKNOWN";

		public ImportJob(
			ILogger<ImportJob> logger,
			ImportService importService,
			ISegmentNormalizerProvider segmentNormalizerProvider,
			ServiceDbContext dbContext)
		{
			_logger = logger;
			_importService = importService;
			_segmentNormalizerProvider = segmentNormalizerProvider;
			_dbContext = dbContext;
		}

		public async Task Execute(IJobExecutionContext context)
		{
			JobDataMap dataMap = context.JobDetail.JobDataMap;

			Configuration configuration = JsonSerializer.Deserialize<Configuration>(dataMap.GetString("configuration"));
			List<TranslationForImport> translationsForImport = JsonSerializer.Deserialize<List<TranslationForImport>>(dataMap.GetString("translationsForImport"));

			try
			{
				await SaveImportedTranslations(translationsForImport, configuration);

				await _importService.CreateOrUpdateImportStatusAsync(configuration.Id, Enums.Import.ImportStatus.SUCCESS);
			}
			catch (TranslationSavingException ex)
			{
				await _importService.CreateOrUpdateImportStatusAsync(configuration.Id, Enums.Import.ImportStatus.FAIL);

				_logger.LogError("Translation saving failed: {Message}", ex.Message);
			}
		}

		public async Task SaveImportedTranslations(List<TranslationForImport> translationsForImport, Configuration configuration)
		{
			var allSegmentMetadata = await _dbContext.SegmentMetadata.AsNoTracking().ToListAsync();
			var allWebsiteSegments = await _dbContext.Segments.Where(segment => segment.WebsiteId == configuration.Id).ToListAsync();
			var allWebsiteSegmentTranslations = await _dbContext.SegmentTranslations
				.Include(segTransl => segTransl.SegmentTranslationHistory)
				.Where(segTransl => segTransl.WebsiteId == configuration.Id).ToListAsync();

			var allWebsiteSegmentTranslationHistories = allWebsiteSegmentTranslations.SelectMany(item => item.SegmentTranslationHistory).ToList();

			try
			{
				foreach (var translationForImport in translationsForImport)
				{

					// checking whether translation target language is indeed present in config language list
					var existingLangDirection = configuration.Languages.Any(ld =>
						ld.TargetLanguage.Equals(translationForImport.TargetLanguage, StringComparison.OrdinalIgnoreCase)
					);

					if (existingLangDirection)
					{
						var metadataId = await GetMetadataSegmentId(
							allSegmentMetadata,
							translationForImport.MetadataTag ?? DefaultTag,
							translationForImport.IsSeo ?? false,
							translationForImport.Attribute ?? null,
							translationForImport.ReferencedAttribute ?? null
						);

						var translation = await AddTranslation(translationForImport);

						var segment = await AddOrGetExistingSegment(allWebsiteSegments, translationForImport, configuration.Id, metadataId);

						await _dbContext.SaveChangesAsync();

						var segmentTranslation = await AddOrUpdateSegmentTranslation(allWebsiteSegmentTranslations, translationForImport, translation, segment.Id, configuration.Id);

						await UpdateSegmentTranslationHistories(allWebsiteSegmentTranslationHistories, segmentTranslation, translation);
					}
				}

				await _dbContext.SaveChangesAsync();
			}
			catch (Exception)
			{
				throw new TranslationSavingException();
			}
		}

		private async Task UpdateSegmentTranslationHistories(
			List<SegmentTranslationHistory> segmentTranslationHistories,
			SegmentTranslation segmentTranslation,
			Translation translation
		)
		{
			SegmentTranslationHistory? existingSegmentTranslationHistory = segmentTranslationHistories.Find(segTranslHist =>
				segTranslHist.SegmentTranslationId == segmentTranslation.Id
			);

			if (existingSegmentTranslationHistory == null)
			{
				var segmentTranslationHistory = new SegmentTranslationHistory()
				{
					Translation = translation,
					SegmentTranslation = segmentTranslation
				};

				await _dbContext.SegmentTranslationHistories.AddAsync(segmentTranslationHistory);
			}
			else
			{
				// updating translation of existing segment translation history
				// needed, when segment translation (id) can be updated via import (new translation added), so the TranslationId should change
				existingSegmentTranslationHistory.Translation = translation;
			}
		}

		private async Task<SegmentTranslation> AddOrUpdateSegmentTranslation(
			List<SegmentTranslation> allWebsiteSegmentTranslations,
			TranslationForImport translationForImport,
			Translation translation,
			long segmentId,
			Guid configId
		)
		{
			SegmentTranslation? existingSegmentTranslation = allWebsiteSegmentTranslations.Find(segTransl =>
					segTransl.SegmentId == segmentId
					&& segTransl.WebsiteTargetLanguageName == translationForImport.TargetLanguage.ToUpper()
					&& segTransl.WebsiteId == configId);

			if (existingSegmentTranslation == null)
			{
				var segmentTranslation = new SegmentTranslation()
				{
					Status = translationForImport.IsAccepted == "0" ? Enums.Segment.SegmentTranslationStatus.New : Enums.Segment.SegmentTranslationStatus.Reviewed,
					AcceptedTranslation = translation,
					SegmentId = segmentId,
					DbCreatedAt = DateTime.UtcNow,
					DbUpdatedAt = DateTime.UtcNow,
					WebsiteId = configId,
					WebsiteTargetLanguageName = translationForImport.TargetLanguage.ToUpper(),
				};

				await _dbContext.SegmentTranslations.AddAsync(segmentTranslation);

				return segmentTranslation;
			}
			else
			{
				existingSegmentTranslation.Status = translationForImport.IsAccepted == "0" ? Enums.Segment.SegmentTranslationStatus.New : Enums.Segment.SegmentTranslationStatus.Reviewed;
				existingSegmentTranslation.AcceptedTranslation = translation;
				existingSegmentTranslation.SegmentId = segmentId;
				existingSegmentTranslation.DbUpdatedAt = DateTime.UtcNow;

				return existingSegmentTranslation;
			}
		}

		private async Task<Translation> AddTranslation(TranslationForImport translationForImport)
		{
			var translation = new Translation()
			{
				Text = translationForImport.TargetText,
				Origin = translationForImport.IsHumanTranslation
				? Enums.Translation.TranslationOrigin.User
				: Enums.Translation.TranslationOrigin.MachineTranslation,
				DbCreatedAt = translationForImport.CreatedTime ?? DateTime.UtcNow,
				DbUpdatedAt = translationForImport.UpdatedTime ?? DateTime.UtcNow,
			};

			await _dbContext.Translations.AddAsync(translation);

			return translation;
		}

		private async Task<Segment> AddOrGetExistingSegment(List<Segment> allSegments, TranslationForImport translationForImport, Guid configId, long metadataId)
		{
			var existingSegment = allSegments.Find(segment =>
				segment.TextNormalized == _segmentNormalizerProvider.Normalize(translationForImport.SourceText) &&
				segment.Uri == translationForImport.UriPath &&
				segment.SegmentMetadataId == metadataId
			);

			if (existingSegment != null)
			{
				return existingSegment;
			}
			else
			{
				var segment = new Segment()
				{
					TextNormalized = _segmentNormalizerProvider.Normalize(translationForImport.SourceText),
					TextRaw = translationForImport.SourceText,
					Uri = translationForImport.UriPath,
					WebsiteId = configId,
					DbCreatedAt = translationForImport.CreatedTime ?? DateTime.UtcNow,
					DbUpdatedAt = translationForImport.UpdatedTime ?? DateTime.UtcNow,
					UriId = _segmentNormalizerProvider.Normalize(translationForImport.UriPath),
					SegmentMetadataId = metadataId
				};
				allSegments.Add(segment);
				await _dbContext.Segments.AddAsync(segment);

				return segment;
			}
		}

		private async Task<long> GetMetadataSegmentId(
			List<SegmentMetadata> allSegmentMetadata,
			string metadataTag,
			bool isSeo,
			string? attribute,
			string? referencedAttribute
		)
		{
			attribute = string.IsNullOrEmpty(attribute) ? null : attribute;
			referencedAttribute = string.IsNullOrEmpty(referencedAttribute) ? null : referencedAttribute;

			var existingMetadata = allSegmentMetadata.Find(item =>
				item.Tag == metadataTag &&
				item.IsSEO == isSeo &&
				item.Attribute == attribute &&
				item.ReferencedAttribute == referencedAttribute
			);

			if (existingMetadata != null)
			{
				return existingMetadata.Id;
			}
			else
			{
				var metadataToSave = new SegmentMetadata()
				{
					IsSEO = isSeo,
					Tag = metadataTag,
					Attribute = attribute,
					ReferencedAttribute = referencedAttribute
				};

				_dbContext.SegmentMetadata.Add(metadataToSave);
				await _dbContext.SaveChangesAsync();

				allSegmentMetadata.Add(metadataToSave);

				return metadataToSave.Id;
			}
		}
	}
}
