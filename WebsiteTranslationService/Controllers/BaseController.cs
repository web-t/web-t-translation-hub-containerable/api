﻿using EnumsNET;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.MT.WebsiteTranslationService.Enums;

namespace Tilde.MT.WebsiteTranslationService.Controllers
{
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// Returns API error in the unified format, providing error code and message
        /// </summary>
        /// <param name="status"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        protected ObjectResult FormatAPIError(HttpStatusCode status, ApiErrorCode errorCode)
        {
            return StatusCode(
                (int)status,
                new APIError()
                {
                    Error = new Error()
                    {
                        Code = (int)status * 1000 + (int)errorCode,
                        Message = errorCode.AsString(EnumFormat.Description)
                    }
                }
            );
        }
    }
}
