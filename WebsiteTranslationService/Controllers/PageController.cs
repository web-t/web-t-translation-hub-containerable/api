using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Page;

namespace Tilde.MT.WebsiteTranslationService.Controllers
{
    [ApiController]
    [Route("translate/website/{websiteId}/pages")]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class PageController : BaseController
    {
        private readonly IPageService _pageService;

        public PageController(
            ILogger<SegmentController> logger,
            IPageService pageService
        )
        {
            _pageService = pageService;
        }

        /// <summary>
        /// List all pages for a website
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="filters"> Filters for paages </param>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(PageList))]
        public async Task<ActionResult<PageList>> ListPages(Guid websiteId, [FromQuery] PageListFilter filters)
        {
            var suggestions = await _pageService.ListPages(User, websiteId, filters);

            return Ok(suggestions);
        }
    }
}