using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.MT.WebsiteTranslationService.Enums;
using Tilde.MT.WebsiteTranslationService.Exceptions;
using Tilde.MT.WebsiteTranslationService.Exceptions.Translation;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.DTO;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Translation;

namespace Tilde.MT.WebsiteTranslationService.Controllers
{
    [ApiController]
    [Route("translate/website/{websiteId}/segments")]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class SegmentController : BaseController
    {
        private readonly ILogger<SegmentController> _logger;
        private readonly ISegmentService _suggestionService;
        public SegmentController(
            ILogger<SegmentController> logger,
            ISegmentService suggestionService
        )
        {
            _logger = logger;
            _suggestionService = suggestionService;
        }

        /// <summary>
        /// List all segments for a website
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="filters"> Filters for segments </param>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(SegmentList))]
        public async Task<ActionResult<SegmentList>> ListSegment(Guid websiteId, [FromQuery] SegmentListFilter filters)
        {
            var suggestions = await _suggestionService.ListSegments(User, websiteId, filters);

            return Ok(suggestions);
        }

        /// <summary>
        /// Delete website segment
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="segmentId"> Segment unique identifier </param>
        /// <returns></returns>
        [Route("{segmentId}")]
        [HttpDelete]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Resource not found", Type = typeof(APIError))]
        public async Task<ActionResult> DeleteSegment(Guid websiteId, long segmentId)
        {
            try
            {
                await _suggestionService.DeleteSegment(User, websiteId, segmentId);

                return Ok();

            }
            catch (ResourceNotFoundException)
            {
                return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
            }
        }

        /// <summary>
        /// Add new translation for a website
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="segmentId"> Segment unique identifier </param>
        /// <param name="lang"> Target language </param>
        /// <param name="suggestion"> New translation suggestion </param>
        /// <returns></returns>
        [Route("{segmentId}/{lang}")]
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(Translation))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Translation is not valid", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Resource not found", Type = typeof(APIError))]
        public async Task<ActionResult<Translation>> AddSegment(
            Guid websiteId,
            long segmentId,
            [MinLength(2)][MaxLength(2)] string lang,
            NewTranslation suggestion
        )
        {
            try
            {
                var translation = await _suggestionService.Add(User, websiteId, segmentId, lang, suggestion.Text, suggestion.Uri);

                return Ok(translation);
            }
            catch (InvalidTranslationException ex)
            {
                _logger.LogError("Translation is not valid, please provide a correct translation");
                return FormatAPIError(HttpStatusCode.BadRequest, Enums.ApiErrorCode.TranslationIsNotValid);
            }
            catch (ResourceNotFoundException ex)
            {
                _logger.LogError(ex, "Cannot add new translation. Please provide existing target language and exisitng segment unique identifier");
                return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
            }
        }

        /// <summary>
        /// Get all translations for a website segment
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="segmentId"> Segment unique identifier </param>
        /// <param name="lang"> Target language </param>
        /// <returns></returns>
        [Route("{segmentId}/{lang}")]
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<Translation>))]
        public async Task<ActionResult<IEnumerable<Translation>>> GetTranslations(
            Guid websiteId,
            long segmentId,
            [MinLength(2)][MaxLength(2)] string lang
        )
        {
            var suggestions = await _suggestionService.ListTranslations(User, websiteId, segmentId, lang);

            return Ok(suggestions);
        }

        /// <summary>
        /// Accept translation for a website segment
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="segmentId"> Segment unique identifier </param>
        /// <param name="lang"> Target language </param>
        /// <param name="translId"> Translation unique identifier </param>
        /// <returns></returns>
        [Route("{segmentId}/{lang}/translation/{translId}")]
        [HttpPut]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Resource not found", Type = typeof(APIError))]
        public async Task<IActionResult> AcceptTranslation(
            Guid websiteId,
            long segmentId,
            [MinLength(2)][MaxLength(2)] string lang,
            long translId
        )
        {
            try
            {
                await _suggestionService.Accept(User, websiteId, lang, segmentId, translId);
                return Ok();
            }
            catch (ResourceNotFoundException ex)
            {
                _logger.LogError(ex, "Translation cannot be accepted, because resource is not found. Please provide existing target language and exisitng segment unique identifier");
                return FormatAPIError(HttpStatusCode.NotFound, ApiErrorCode.ResourceNotFound);
            }
        }

        /// <summary>
        /// Delete translation from a website segment
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="segmentId"> Segment unique identifier </param>
        /// <param name="lang"> Target language </param>
        /// <param name="translId"> Translation unique identifier </param>
        /// <returns></returns>
        [Route("{segmentId}/{lang}/{translId}")]
        [HttpDelete]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Resource not found", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Cannot delete accepted translation", Type = typeof(APIError))]
        public async Task<IActionResult> DeleteTranslation(
            Guid websiteId,
            long segmentId,
            [MinLength(2)][MaxLength(2)] string lang,
            long translId
        )
        {
            try
            {
                await _suggestionService.Remove(User, websiteId, segmentId, lang, translId);
                return Ok();
            }
            catch (ResourceNotFoundException ex)
            {
                _logger.LogError(ex, "Translation is not found, please provide correct website unique identifier, segment unique identifier, target language, and translation unique identifier");
                return FormatAPIError(HttpStatusCode.NotFound, ApiErrorCode.ResourceNotFound);
            }
            catch (AcceptedTranslationDeleteNotAllowedException ex)
            {
                _logger.LogError(ex, "Cannot delete translation because it is accepted");
                return FormatAPIError(HttpStatusCode.BadRequest, ApiErrorCode.CannotDeleteAcceptedTranslation);
            }
        }
    }
}