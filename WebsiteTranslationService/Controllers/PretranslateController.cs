﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.MT.WebsiteTranslationService.Exceptions.Pretranslation;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.Pretranslation;
using Tilde.MT.WebsiteTranslationService.Services;

namespace Tilde.MT.WebsiteTranslationService.Controllers
{
	[ApiController]
	[Route("pretranslate")]
	[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
	public class PretranslateController : BaseController
	{
		private readonly ILogger<PretranslateController> _logger;
		private readonly IConfigurationService _configurationService;
		private readonly PretranslateService _pretranslateService;

		public PretranslateController(
			ILogger<PretranslateController> logger,
			IConfigurationService configurationService,
			PretranslateService pretranslateService
		)
		{
			_logger = logger;
			_configurationService = configurationService;

			_pretranslateService = pretranslateService;
		}

		/// <summary>
		/// Start website pretranslation
		/// </summary>
		/// <param name="pretranslationRequest"> Website id, domain to pretranslate and target languages into which to pretranslate </param>
		/// <returns></returns>
		[Route("start-pretranslation")]
		[HttpPost]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Configuration not found", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Could not start website pretranslation due to invalid/missing information", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error while starting website pretranslation", Type = typeof(APIError))]
		public async Task<ActionResult> PretranslateWebsite([FromBody] PretranslationRequest pretranslationRequest)
		{
			try
			{
				await _pretranslateService.StartWebsitePretranslation(pretranslationRequest);

				return Ok(true);
			}
			catch (InvalidParametersException)
			{
				return FormatAPIError(HttpStatusCode.BadRequest, Enums.ApiErrorCode.InvalidPretranslationParametersError);
			}
			catch (ConfigurationNotFoundException)
			{
				return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
			}
			catch (Exception ex)
			{
				_logger.LogError($"Could not start website pretranslation. Configuration ID: {pretranslationRequest.ConfigurationId}. Exception: {ex.Message}");

				return FormatAPIError(HttpStatusCode.InternalServerError, Enums.ApiErrorCode.PretranslationStartError);
			}
		}

		/// <summary>
		/// Retrieve status of website pretranslation
		/// </summary>
		/// <param name="configurationId"> Website unique identifier </param>
		/// <returns></returns>
		[Route("{configurationId}/status")]
		[HttpGet]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Configuration not found", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Could not retreive valid website pretranslation status", Type = typeof(APIError))]
		public async Task<ActionResult> GetWebsitePretranslationStatus(Guid configurationId)
		{
			try
			{
				var statusResponse = await _pretranslateService.GetPretranslationStatus(configurationId);

				return Ok(statusResponse);
			}
			catch (ConfigurationNotFoundException)
			{
				return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
			}
			catch (HttpRequestException ex)
			{
				_logger.LogError($"Could not retrieve pretranslation status. Configuration ID: {configurationId}. Exception: {ex.Message}");

				return FormatAPIError(HttpStatusCode.BadRequest, Enums.ApiErrorCode.PretranslationStatusRetrieveError);
			}
		}

		/// <summary>
		/// Start website pretranslation retry
		/// </summary>
		/// <param name="configurationId"> Website unique identifier </param>
		/// <returns></returns>
		[Route("retry-pretranslation")]
		[HttpPost]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Configuration not found", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error while starting website pretranslation retry", Type = typeof(APIError))]
		public async Task<ActionResult> RetryWebsitePretranslate(Guid configurationId)
		{
			try
			{
				await _pretranslateService.StartWebsitePretranslationRetry(configurationId);

				return Ok(true);
			}
			catch (ConfigurationNotFoundException)
			{
				return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
			}
			catch (Exception ex)
			{
				_logger.LogError($"Could not start website pretranslation. Configuration ID: {configurationId}. Exception: {ex.Message}");

				return FormatAPIError(HttpStatusCode.InternalServerError, Enums.ApiErrorCode.PretranslationStartError);
			}
		}

		/// <summary>
		/// Reset pretranslation status of configuration
		/// </summary>
		/// <param name="configurationId"> Website unique identifier </param>
		/// <returns></returns>
		[Route("reset-pretranslation-status")]
		[HttpPost]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Configuration not found", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error while resetting website pretranslation status", Type = typeof(APIError))]
		public async Task<ActionResult> ResetWebsitePretranslationStatus(Guid configurationId)
		{
			try
			{
				await _pretranslateService.ResetWebsitePretranslationStatus(configurationId);

				return Ok(true);
			}
			catch (ConfigurationNotFoundException)
			{
				return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
			}
			catch (Exception ex)
			{
				_logger.LogError($"Could not reset pretranslation status. Configuration ID: {configurationId}. Exception: {ex.Message}");

				return FormatAPIError(HttpStatusCode.InternalServerError, Enums.ApiErrorCode.PretranslationStatusResetError);
			}
		}


		/// <summary>
		/// Cancel website pretranslation
		/// </summary>
		/// <param name="configurationId"> Website unique identifier </param>
		/// <returns></returns>
		[Route("cancel-pretranslation")]
		[HttpPost]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Configuration not found", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error while canceling website pretranslation", Type = typeof(APIError))]
		public async Task<ActionResult> CancelWebsitePretranslate(Guid configurationId)
		{
			try
			{
				await _pretranslateService.CancelWebsitePretranslation(configurationId);

				return Ok(true);
			}
			catch (ConfigurationNotFoundException)
			{
				return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
			}
			catch (Exception ex)
			{
				_logger.LogError($"Could not cancel website pretranslation. Configuration ID: {configurationId}. Exception: {ex.Message}");

				return FormatAPIError(HttpStatusCode.InternalServerError, Enums.ApiErrorCode.PretranslationCancelError);
			}
		}
	}
}
