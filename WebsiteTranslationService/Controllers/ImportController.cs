﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.MT.WebsiteTranslationService.Enums.Import;
using Tilde.MT.WebsiteTranslationService.Exceptions.Import;
using Tilde.MT.WebsiteTranslationService.Exceptions.Pretranslation;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;
using Tilde.MT.WebsiteTranslationService.Models.TranslationImport;
using Tilde.MT.WebsiteTranslationService.Services;

namespace Tilde.MT.WebsiteTranslationService.Controllers
{
	[ApiController]
	[Route("import")]
	[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
	public class ImportController : BaseController
	{
		private readonly ILogger<ImportController> _logger;
		private readonly ImportService _importService;
		private readonly IConfigurationService _configurationService;

		public ImportController(ILogger<ImportController> logger, ImportService importService, IConfigurationService configurationService)
		{
			_logger = logger;
			_importService = importService;
			_configurationService = configurationService;
		}

		/// <summary>
		/// Import XLIFF file of client domain translations
		/// </summary>
		/// <param name="importFields"> XLIFF File and Config ID </param>
		/// <returns></returns>
		[Route("")]
		[HttpPost]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.Conflict, Description = "Translation import in progress, cannot start another import", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Error while parsing XLIFF file", Type = typeof(APIError))]
		[SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error saving translations to database", Type = typeof(APIError))]
		public async Task<IActionResult> ImportWebsiteTranslations([FromForm] ImportFields importFields)
		{
			try
			{
				await _importService.ImportTranslations(importFields.xliffFile, importFields.configId);

				return Ok();
			}
			catch (ImportAlreadyInProgressException ex)
			{
				_logger.LogError("Import already in progress: {Message}", ex.Message);
				return FormatAPIError(HttpStatusCode.Conflict, Enums.ApiErrorCode.ImportAlreadyInProgressError);
			}
			catch (FileTooLargeException ex)
			{
				_logger.LogError("File too large: {Message}", ex.Message);
				return FormatAPIError(HttpStatusCode.BadRequest, Enums.ApiErrorCode.CATFileParsingError);
			}
			catch (FileParsingException ex)
			{
				_logger.LogError("File parsing failed: {Message}", ex.Message);
				return FormatAPIError(HttpStatusCode.BadRequest, Enums.ApiErrorCode.CATFileParsingError);
			}
		}

		/// <summary>
		/// Create or update translation import status for specified configuration
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <param name="status"> Translation import status </param>
		/// <returns></returns>
		[HttpPost]
		[Route("{id}/import-status")]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Configuration not found", Type = typeof(APIError))]
		public async Task<ActionResult<ImportStatus>> CreateOrUpdateImportStatus(Guid id, [FromQuery] ImportStatus status)
		{
			try
			{
				await _importService.CreateOrUpdateImportStatusAsync(id, status);

				return Ok();
			}
			catch (ConfigurationNotFoundException ex)
			{
				_logger.LogError(ex, "Configuration not found, please provide existing configuration unique identifier");

				return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
			}
		}

		/// <summary>
		/// Retrieve translation import status of configuration
		/// </summary>
		/// <param name="id"> Configuration unique identifier </param>
		/// <returns></returns>
		[HttpGet]
		[Route("{id}/import-status")]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Configuration not found", Type = typeof(APIError))]
		public async Task<ActionResult<ImportStatus>> GetImportStatus(Guid id)
		{
			try
			{
				var result = await _importService.GetImportStatusAsync(id);

				return Ok(result);
			}
			catch (ConfigurationNotFoundException ex)
			{
				_logger.LogError(ex, "Configuration not found, please provide existing configuration unique identifier");

				return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.ResourceNotFound);
			}
		}
	}
}
