﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.MT.WebsiteTranslationService.Models.DTO.Translation;
using Tilde.MT.WebsiteTranslationService.Services;

namespace Tilde.MT.WebsiteTranslationService.Controllers
{
	[ApiController]
	[Route("export")]
	[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
	public class ExportController : BaseController
	{
		private readonly ILogger _logger;
		private readonly ExportService _exportService;

		public ExportController(ILogger<ExportController> logger, ExportService exportService)
		{
			_logger = logger;

			_exportService = exportService;
		}

		/// <summary>
		/// Returns XLIFF file of translations for selected website
		/// </summary>
		/// <param name="configId"> Config (website) ID </param>
		/// <param name="filters"> Filters to filter segment translations before export </param>
		/// <returns></returns>
		[Route("")]
		[HttpGet]
		[SwaggerResponse((int)HttpStatusCode.OK)]
		[SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "Error while exporting translations", Type = typeof(APIError))]
		public async Task<ActionResult> ExportWebsiteTranslations([Required][FromQuery] Guid configId, [FromQuery] SegmentListFilterForExport filters)
		{
			try
			{
				var byteContent = await _exportService.ExportWebsiteTranslations(configId, filters);

				return File(byteContent, "application/xml", $"translations_{configId}.xliff");
			}
			catch (Exception ex)
			{
				_logger.LogError("Translation export failed: {Message}", ex.Message);

				return FormatAPIError(HttpStatusCode.InternalServerError, Enums.ApiErrorCode.CATTranslationExportError);
			}
		}
	}
}
