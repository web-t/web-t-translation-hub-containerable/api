﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.MT.WebsiteTranslationService.Exceptions.Domain;
using Tilde.MT.WebsiteTranslationService.Interfaces.Services;

namespace Tilde.MT.WebsiteTranslationService.Controllers
{
    [ApiController]
    [Route("translate/website/{websiteId}/translate")]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class TranslationController : BaseController
    {
        private readonly ILogger _logger;
        private readonly ITranslationService _textService;

        public TranslationController(
            ILogger<TranslationController> logger,
            ITranslationService textService
        )
        {
            _logger = logger;
            _textService = textService;
        }

        /// <summary>
        /// Get website segment translation
        /// </summary>
        /// <param name="websiteId"> Website unique identifier </param>
        /// <param name="request"> Segment request </param>
        /// <returns></returns>
        [Route("")]
        [HttpPost]
        [AllowAnonymous]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(IEnumerable<Models.DTO.Translation.TranslationItem>))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Language direction not found", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.GatewayTimeout, Description = "Translation timeout", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Resource not found", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Domain not allowed", Type = typeof(APIError))]
        public async Task<ActionResult<IEnumerable<Models.DTO.Translation.TranslationItem>>> Get(Guid websiteId, Models.DTO.Translation.SegmentRequest request)
        {
            var header = Request.GetTypedHeaders();

            try
            {
                var translations = await _textService.Translate(websiteId, request, header.Referer);

                return Ok(translations);
            }
            catch (LanguageDirectionNotFoundException ex)
            {
                _logger.LogError(ex, "Language direction not found for website '{Website}'", websiteId);
                return FormatAPIError(HttpStatusCode.NotFound, Enums.ApiErrorCode.LanguageDirectionNotFound);
            }
            catch (TranslationTimeoutException ex)
            {
                _logger.LogError("Translation timeout: {Error}", ex.Message);
                return FormatAPIError(HttpStatusCode.GatewayTimeout, Enums.ApiErrorCode.TranslationTimeout);
            }
            catch (DomainNotAllowedException ex)
            {
                _logger.LogError(ex, "Domain not allowed");
                return FormatAPIError(HttpStatusCode.BadRequest, Enums.ApiErrorCode.DomainNotAllowed);
            }
        }
    }
}
