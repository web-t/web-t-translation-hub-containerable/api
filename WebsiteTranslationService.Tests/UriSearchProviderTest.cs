using FluentAssertions;
using Tilde.MT.WebsiteTranslationService.Providers;

namespace WebsiteTranslationService.Tests
{
    public class UriSearchProviderTest
    {
        [Theory]
        [InlineData("https://tilde.com/products-and-services/machine-translation#client?id=2552&client=chess24", new string[] { "https://tilde.com" }, "/products-and-services/machine-translation", true)]
        [InlineData("https://tilde.com/products-and-services/machine-translation", new string[] { "https://tilde.com" }, "/products-and-services/machine-translation", true)]
        [InlineData("tilde.com/products-and-services/machine-translation", new string[] { "https://tilde.com" }, "/products-and-services/machine-translation", true)]
        [InlineData("www.tilde.com/products-and-services/machine-translation", new string[] { "https://tilde.com" }, "/products-and-services/machine-translation", false)]
        [InlineData("google.com/products-and-services/machine-translation", new string[] { "https://tilde.com" }, "/products-and-services/machine-translation", false)]
        [InlineData("/products-and-services/machine-translation#client?id=2552&client=chess24", new string[] { "https://tilde.com" }, "/products-and-services/machine-translation", true)]
        [InlineData("/products-and-services/machine-translation", new string[] { "https://tilde.com" }, "/products-and-services/machine-translation", true)]
        [InlineData("ftp://foo/bar", new string[] { "https://tilde.com" }, "/bar", false)]
        public void Test(string uri, ICollection<string> allowedDomains, string expectedNormalizedUri, bool expectedCanSearch)
        {
            var provider = new UriSearchProvider();

            var canSearch = provider.CanSearch(uri, allowedDomains, out string? normalizedUri);

            canSearch.Should().Be(normalizedUri != null);
            canSearch.Should().Be(expectedCanSearch);

            if (canSearch)
            {
                normalizedUri.Should().Be(expectedNormalizedUri);
            }
        }
    }
}