# Website Translation Service

Provides API that can be used to create, manage website translation integrations and request translations 

## Terminology

`translation system` - source language + target language + domain

## Architecture

```

 -----------------------------------             --------------------------------
|                                   |           |                                |
|     Translation system service    |   → → →   |           Database             |
|                                   |   ← ← ←   |                                |
 -----------------------------------             --------------------------------
       
                ↓
                ↓
                ↓
                ↓
                ↓
                ↓   Request text translations if not cached
                ↓                   ----------------------------------- 
                ↓                   |                                   |    
                → → → → → → → → →   |     Text Translation service      |  
                ↓                   |                                   |  
                ↓                   -----------------------------------    
                ↓
                ↓
                ↓
                ↓   Request available systems for validation
                ↓                   ----------------------------------- 
                ↓                   |                                   |    
                → → → → → → → → →   |         System service            |  
                                    |                                   |  
                                    -----------------------------------    


```
<!-- 
# Monitor

## Healthcheck probes

https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/

Startup probe / Readiness probe:

`/health/ready`

Liveness probe:

`/health/live` -->


# Test

Using docker compose
```
docker-compose up --build
```

## Benchmark

### Vegeta
```ps1
# Install vegeta
go install github.com/tsenart/vegeta@latest

# Run benchmark
vegeta attack -targets=vegeta_translate -format=http -duration=30s -rate=500 --output results.bin
vegeta report -type=text results.bin
vegeta plot results.bin  > plot.html
```

### k6
```ps1
# influx v2 output plugin
xk6 build --with github.com/grafana/xk6-output-influxdb

./k6 run script.js --vus 800 --duration 30s -o xk6-influxdb=http://localhost:8086
```